// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::time::Instant;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use imgui::{Context, FontConfig,FontSource, MouseCursor};
use pixels::{raw_window_handle::HasRawWindowHandle, wgpu, PixelsContext};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use super::{Application, Input};


// ImGUI Wrapper --------------------------------------------------------------
// ----------------------------------------------------------------------------
pub(crate) struct Im {
    imgui: Context,
    platform: imgui_winit_support::WinitPlatform,
    renderer: imgui_wgpu::Renderer,
    last_frame: Instant,
    last_cursor: Option<MouseCursor>
}

impl Im {
    pub(crate) fn new<W: HasRawWindowHandle>(
        window: &winit::window::Window,
        pixels: &pixels::Pixels<W>,

    ) -> Self {
        // Create Dear ImGui context
        let mut imgui = Context::create();
        imgui.set_ini_filename(None);

        // Initialize winit platform support
        let mut platform = imgui_winit_support::WinitPlatform::init(&mut imgui);
        platform.attach_window(
            imgui.io_mut(),
            &window,
            imgui_winit_support::HiDpiMode::Default,
        );

        // Configure Fonts
        let hidpi_factor = window.scale_factor().round();
        let font_size = (13.0 * hidpi_factor) as f32 * 2.0;
        imgui.io_mut().font_global_scale = (1.0 / hidpi_factor) as f32 / 2.0;
        imgui
            .fonts()
            .add_font(&[FontSource::DefaultFontData {
                config: Some(FontConfig {
                    oversample_h: 3,
                    oversample_v: 3,
                    pixel_snap_h: false,
                    size_pixels: font_size,
                    ..Default::default()
                }),
            }]);

        // Fix incorrect colors with sRGB framebuffer
        let style = imgui.style_mut();
        for color in 0..style.colors.len() {
            style.colors[color] = gamma_to_linear(style.colors[color]);
        }

        // Create WGPU renderer
        let device = pixels.device();
        let queue = pixels.queue();
        let config = imgui_wgpu::RendererConfig::new().set_texture_format(wgpu::TextureFormat::Bgra8UnormSrgb);
        let renderer = imgui_wgpu::Renderer::new(&mut imgui, &device, &queue, config);

        Self {
            imgui,
            platform,
            renderer,
            last_frame: Instant::now(),
            last_cursor: None
        }
    }

    pub(crate) fn prepare(
        &mut self,
        window: &winit::window::Window,

    ) -> Result<(), winit::error::ExternalError> {
        let io = self.imgui.io_mut();
        io.update_delta_time(self.last_frame.elapsed());
        self.last_frame = Instant::now();
        self.platform.prepare_frame(io, window)
    }

    pub(crate) fn render<T: 'static + Application>(
        &mut self,
        window: &winit::window::Window,
        input: &Input,
        encoder: &mut wgpu::CommandEncoder,
        render_target: &wgpu::TextureView,
        context: &PixelsContext,
        application: &mut T

    ) -> imgui_wgpu::RendererResult<()> {
        let ui = self.imgui.frame();

        let mouse_cursor = ui.mouse_cursor();
        if self.last_cursor != mouse_cursor {
            self.last_cursor = mouse_cursor;
            self.platform.prepare_render(&ui, window);
        }

        // Draw Application UI
        application.draw_ui(&ui, input);

        // Render ImGui with WGPU
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                attachment: render_target,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Load,
                    store: true,
                },
            }],
            depth_stencil_attachment: None,
        });

        self.renderer.render(ui.render(), &context.queue, &context.device, &mut rpass)
    }

    pub(crate) fn handle_event(
        &mut self,
        window: &winit::window::Window,
        event: &winit::event::Event<()>,
    ) {
        self.platform .handle_event(self.imgui.io_mut(), window, event);
    }
}

pub fn gamma_to_linear(color: [f32; 4]) -> [f32; 4] {
    const GAMMA: f32 = 2.2;
    let x = color[0].powf(GAMMA);
    let y = color[1].powf(GAMMA);
    let z = color[2].powf(GAMMA);
    let w = 1.0 - (1.0 - color[3]).powf(GAMMA);
    [x, y, z, w]
}

