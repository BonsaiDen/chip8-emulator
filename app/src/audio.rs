// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::sync::{Arc, Mutex};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use cpal::{Sample, SampleFormat, SampleRate, Stream};
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const AUDIO_BUFFER_SIZE: usize = 8192;


// Enums ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
type AudioBufferLock = Arc<Mutex<AudioBuffer>>;
struct AudioBuffer {
    read_index: usize,
    write_index: usize,
    data: [f32; AUDIO_BUFFER_SIZE]
}

pub struct AudioStream {
    buffer: AudioBufferLock,
    _stream: Stream
}

impl AudioStream {
    pub fn new(sample_rate: u32) -> Result<AudioStream, String> {
        let buffer = Arc::new(Mutex::new(AudioBuffer {
            read_index: 0,
            write_index: 0,
            data: [0.0; AUDIO_BUFFER_SIZE]
        }));

        let host = cpal::default_host();
        let device = host.default_output_device().ok_or_else(|| "".to_string())?;
        let mut supported_configs_range = device.supported_output_configs().map_err(|e| e.to_string())?;
        let supported_config = supported_configs_range.next().ok_or_else(|| "".to_string())?
            .with_sample_rate(SampleRate(sample_rate));

        let err_fn = |err| eprintln!("an error occurred on the output audio stream: {}", err);
        let sample_format = supported_config.sample_format();
        let config = supported_config.into();
        let inner_buffer = buffer.clone();
        let stream = match sample_format {
            SampleFormat::F32 => device.build_output_stream(&config, move |data: &mut [f32], c: &cpal::OutputCallbackInfo| Self::write_data(&inner_buffer, data, c), err_fn),
            SampleFormat::I16 => device.build_output_stream(&config, move |data: &mut [i16], c: &cpal::OutputCallbackInfo| Self::write_data(&inner_buffer, data, c), err_fn),
            SampleFormat::U16 => device.build_output_stream(&config, move |data: &mut [u16], c: &cpal::OutputCallbackInfo| Self::write_data(&inner_buffer, data, c), err_fn),
        }.unwrap();

        stream.play().map_err(|e| e.to_string())?;
        Ok(AudioStream {
            buffer,
            _stream: stream
        })
    }

    fn write_data<T: Sample>(audio_buffer: &AudioBufferLock, data: &mut [T], _: &cpal::OutputCallbackInfo) {
        if let Ok(mut buffer) = audio_buffer.lock() {
            for sample in data.iter_mut() {
                let index = buffer.read_index;
                *sample = Sample::from(&buffer.data[index]);
                buffer.data[index] = 0.0;
                buffer.read_index = (buffer.read_index + 1) % AUDIO_BUFFER_SIZE;
            }
        }
    }

    pub fn handle_events(&self, events: Vec<AudioEvent>) {
        if let Ok(mut buffer) = self.buffer.lock() {
            for event in events {
                event.handle(&mut buffer);
            }
        }
    }

}

#[derive(Debug)]
pub enum AudioEvent {
    Dequeue(usize),
    Queue(usize, Vec<f32>)
}

impl AudioEvent {
    fn handle(self, buffer: &mut std::sync::MutexGuard<AudioBuffer>) {
        match self {
            AudioEvent::Queue(samples, data) => {
                for i in 0..samples.min(data.len()) {
                    let index = buffer.write_index;
                    buffer.data[index] = data[i];
                    buffer.write_index = (buffer.write_index + 1) % AUDIO_BUFFER_SIZE;
                }
            },
            AudioEvent::Dequeue(samples) => {
                // Remove the samples from the write buffer
                for _ in 0..samples {
                    let index = buffer.write_index;
                    buffer.data[index] = 0.0;
                    if buffer.write_index > 0 {
                        buffer.write_index = buffer.write_index - 1;

                    } else {
                        buffer.write_index = AUDIO_BUFFER_SIZE - 1;
                    }
                }
                // Correct read pointer to match up with the most recent write
                if buffer.read_index > buffer.write_index {
                    if buffer.read_index - buffer.write_index < AUDIO_BUFFER_SIZE / 2 {
                        buffer.read_index = buffer.write_index;
                    }

                } else {
                    if buffer.write_index - buffer.read_index > AUDIO_BUFFER_SIZE / 2 {
                        buffer.read_index = buffer.write_index;
                    }
                }
            }
        }
    }
}

