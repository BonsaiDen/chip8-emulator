// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::time::Instant;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use winit::event::Event;
pub use winit::dpi::LogicalSize;
use winit::window::WindowBuilder;
pub use winit::window::{Fullscreen, Window};
use winit::event_loop::{ControlFlow, EventLoop};
use pixels::{Error, PixelsBuilder, SurfaceTexture};
pub use winit_input_helper::WinitInputHelper as Input;
pub use winit::event::VirtualKeyCode as Key;


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod audio;
pub use self::audio::AudioEvent;
use self::audio::AudioStream;
mod im;
pub use self::im::gamma_to_linear;
pub use imgui;


// Application Wrapper --------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait Application {
    fn draw_pixels(&mut self, frame: &mut [u8]);
    fn draw_ui(&mut self, imgui: &imgui::Ui, input: &Input);
    fn audio_events(&mut self) -> Vec<AudioEvent>;
    fn init(&mut self, window: &Window);
    fn update(&mut self, window: &Window, input: &Input, dt: f32) -> Result<bool, String>;
    fn run(
        title: &'static str,
        width: u32,
        height: u32,
        buffer_width: u32,
        buffer_height: u32,
        sample_rate: Option<u32>,
        app: Self

    ) where Self: 'static + Sized {
        run(title, width, height, buffer_width, buffer_height, sample_rate, app).expect("Application failed");
    }
}

fn run<T: 'static + Application>(
    title: &'static str,
    width: u32,
    height: u32,
    buffer_width: u32,
    buffer_height: u32,
    sample_rate: Option<u32>,
    mut application: T

) -> Result<(), Error> {

    let event_loop = EventLoop::new();
    let mut input = Input::new();
    let window = {
        let size = LogicalSize::new(width, height);
        WindowBuilder::new()
            .with_title(title)
            .with_inner_size(size)
            .with_min_inner_size(size)
            .build(&event_loop)
            .unwrap()
    };

    let mut pixels = {
        let window_size = window.inner_size();
        let surface_texture = SurfaceTexture::new(window_size.width, window_size.height, &window);
        PixelsBuilder::new(buffer_width, buffer_height, surface_texture).enable_vsync(true).build()?
    };

    let audio_stream = if let Some(sample_rate) = sample_rate {
        AudioStream::new(sample_rate).ok()

    } else {
        None
    };

    application.init(&window);

    let mut now = Instant::now();
    let mut im_gui = im::Im::new(&window, &pixels);
    event_loop.run(move |event, _, control_flow| {

        // Drawing
        if let Event::RedrawRequested(_) = event {
            application.draw_pixels(pixels.get_frame());
            im_gui.prepare(&window).expect("im_gui.prepare() failed");

            let render_result = pixels.render_with(|encoder, render_target, context| {
                context.scaling_renderer.render(encoder, render_target);
                im_gui.render(
                    &window,
                    &input,
                    encoder,
                    render_target,
                    context,
                    &mut application

                ).expect("im_gui.render() failed");
            });

            if render_result.is_err() {
                *control_flow = ControlFlow::Exit;
                return;
            }
        }

        // Events
        im_gui.handle_event(&window, &event);
        if input.update(event) {

            // Delta Time
            let dt = now.elapsed().as_secs_f32();
            now = Instant::now();

            // Update Application
            if input.quit() || application.update(&window, &mut input, dt).expect("application.update() failed") == false {
                *control_flow = ControlFlow::Exit;
                return;
            }

            // Resize the window
            if let Some(size) = input.window_resized() {
                pixels.resize(size.width, size.height);
            }

            // Audio
            let events = application.audio_events();
            if let Some(stream) = audio_stream.as_ref() {
                stream.handle_events(events);
            }

            // Update internal state and request a redraw
            window.request_redraw();
        }
    });
}

