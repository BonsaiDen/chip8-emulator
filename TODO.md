# TODOs

- Sound Beeps
- Missing OpCodes

- Application
    - Make use of delta time when calculation cycles per frame

- Debugger UI

    - Support fixed keyboard inputs in debugger
        - emulate keyboard with buttons that can be toggled

    - Support adding / toggle / removing breakpoints
        Active    Addr     Read    Write         [Delete]
        Yes       0x2A0    Yes A2  No --         [X]

    - Serialize / Store Debugger UI State
    - Store debugger session state in a file

    - Support giving names to addresses 
        - Extra Tab for Display / Removal / Editing of Labels

