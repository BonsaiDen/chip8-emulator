// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::path::PathBuf;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use structopt::StructOpt;


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod debugger;
mod display;
mod emulator;
mod memory;
mod vm;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use self::emulator::Emulator;


// Application Entry-----------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, StructOpt)]
#[structopt(name = "ch8", about = "A CHIP-8 Emulator and Debugger.")]
struct Arguments {
    /// Start with Debugger active
    #[structopt(short, long)]
    debug: bool,

    /// Set scaling factor
    #[structopt(short, long, default_value="1")]
    scale: u8,

    /// ROM file
    #[structopt(parse(from_os_str))]
    rom_file: Option<PathBuf>
}

fn main() {
    let mut args = Arguments::from_args();
    let mut emulator = Emulator::new(args.scale.min(3) as u32);

    if args.debug {
        emulator.debug();
    }

    if let Some(rom_file) = args.rom_file.take() {
        emulator.load_rom_from_path(PathBuf::from(rom_file)).expect("Failed to load ROM");
    }

    emulator.start();
}

