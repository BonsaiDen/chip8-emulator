// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::debugger::Entry;
use crate::memory::{MAX_PROGRAM_ADDRESS, MemoryAddress, Register};
use crate::vm::Instruction;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub type AnalyzerJump = (usize, bool, usize, bool, usize, MemoryAddress, MemoryAddress, usize, bool);
pub type AnalyzerCall = (usize, MemoryAddress, usize, MemoryAddress, bool, Vec<(usize, usize)>);

#[derive(Debug, Copy, Clone)]
pub enum AnalyzerEntry {
    Instruction(Instruction, bool),
    InstructionLong(MemoryAddress, bool),
    DataByte(u8),
    RegisterByte(Register, u8),
    None
}

impl AnalyzerEntry {
    fn jump_target(&self) -> Option<MemoryAddress> {
        match self {
            Self::Instruction(i, _) => i.jump_target(),
            _ => None
        }
    }

    fn call_target(&self) -> Option<MemoryAddress> {
        match self {
            Self::Instruction(i, _) => i.call_target(),
            _ => None
        }
    }

    fn is_return(&self) -> bool {
        match self {
            Self::Instruction(i, _) => i.is_return(),
            _ => false
        }
    }

    fn is_skip(&self) -> bool {
        match self {
            Self::Instruction(i, _) => i.is_skip(),
            _ => false
        }
    }

    fn is_instruction(&self) -> bool {
        match self {
            Self::Instruction(_, _) => true,
            _ => false
        }
    }

    fn is_guessed(&self) -> bool {
        match self {
            Self::Instruction(_, guessed) => *guessed,
            Self::InstructionLong(_, guessed) => *guessed,
            _ => false
        }
    }

    fn len(&self) -> u16 {
        match self {
            Self::Instruction(_, _) => 2,
            Self::InstructionLong(_, _) => 4,
            Self::DataByte(_) => 1,
            Self::RegisterByte(_, _) => 1,
            Self::None => 1
        }
    }
}


// Analyzer Implementation ----------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Analyzer {
    entries: HashMap<MemoryAddress, AnalyzerEntry>,
    overwrites: HashMap<MemoryAddress, bool>,
    invalid: HashSet<MemoryAddress>,
    address_to_line: HashMap<MemoryAddress, usize>,
    lines: Vec<(MemoryAddress, AnalyzerEntry)>,
    jumps: Vec<(usize, MemoryAddress, MemoryAddress, usize, bool)>,
    calls: Vec<AnalyzerCall>,
}

impl Analyzer {
    pub fn new() -> Self {
        Self {
            entries: HashMap::new(),
            overwrites: HashMap::new(),
            invalid: HashSet::new(),
            address_to_line: HashMap::new(),
            lines: Vec::new(),
            jumps: Vec::new(),
            calls: Vec::new()
        }
    }

    pub fn invalidate(&mut self, addr: MemoryAddress)  {
        self.invalid.insert(addr);
    }

    pub fn mark_as_data(&mut self, addr: MemoryAddress) {
        if let Some(e) = self.entries.get(&addr) {
            match e {
                AnalyzerEntry::Instruction(_, _) => {
                    self.overwrites.insert(addr, true);
                    self.entries.remove(&addr);
                    self.overwrites.insert(addr.saturating_add(1), true);
                    self.entries.remove(&addr.saturating_add(1));
                },
                _ => {}
            }
        }
    }

    pub fn get_target(&self, addr: MemoryAddress) -> Option<MemoryAddress> {
        if let Some(AnalyzerEntry::Instruction(i, _)) = self.entries.get(&addr) {
            i.jump_target().or(i.call_target())

        } else {
            None
        }
    }

    pub fn snap_address(&self, addr: MemoryAddress) -> MemoryAddress {
        if self.entries.get(&addr).is_some() {
            addr

        } else if self.entries.get(&addr.saturating_sub(1)).is_some() {
            addr.saturating_sub(1)

        } else if self.entries.get(&addr.saturating_sub(2)).is_some() {
            addr.saturating_sub(2)

        } else {
            addr
        }
    }

    pub fn get_size(&self, addr: MemoryAddress) -> u16 {
        if let Some(e) = self.entries.get(&addr) {
            e.len()

        } else if let Some(e) = self.entries.get(&addr.saturating_sub(1)) {
            e.len()

        } else if let Some(e) = self.entries.get(&addr.saturating_sub(2)) {
            e.len()

        } else {
            0
        }
    }

    pub fn update(&mut self, entries: &HashMap<MemoryAddress, Entry>, cursor: MemoryAddress, length: usize) -> (
        usize,
        Vec<(MemoryAddress, AnalyzerEntry)>,
        Vec<AnalyzerJump>,
        Vec<AnalyzerCall>
    ) {
        let cursor = self.analyze_instructions(entries, cursor);
        let mut lines = Vec::new();
        let mut height = 0;
        let mut line_offset = None;
        let min_l = cursor.saturating_sub(length / 2);
        let max_l = cursor.saturating_add(length.saturating_sub(length / 2));

        // Compute visible lines around current view cursor
        for (i, line) in self.lines.iter().enumerate() {
            let visible = if i > max_l {
                lines.len() < length

            } else  {
                i >= min_l && i <= max_l
            };
            if visible {
                if line_offset.is_none() {
                    line_offset = Some(i);
                }
                lines.push(*line);
                height += 1;
            }
        }

        let jumps = self.analyze_jumps(height, line_offset);
        let calls = self.filter_calls(height, line_offset);
        (self.lines.len(), lines, jumps, calls)
    }

    fn analyze_instructions(&mut self, entries: &HashMap<MemoryAddress, Entry>, cursor: MemoryAddress) -> usize {
        // TODO better caching for reduced re-compute
        self.lines.clear();
        self.jumps.clear();
        self.calls.clear();
        self.address_to_line.clear();

        // Clear invalidated entry data
        for i in self.invalid.drain() {
            self.entries.remove(&i);
        }

        let mut call_targets = HashMap::new();
        let mut jump_targets = HashMap::new();
        let mut line = 0;
        let mut min_distance = 0xFFFF;
        let mut addr = 0x0;
        while addr < 0x0EAF {
            let entry = self.address_entry(entries, addr);

            // Record jumps
            if let Some(target_addr) = entry.jump_target() {
                jump_targets.entry(target_addr).or_insert_with(Vec::new).push(self.jumps.len());
                self.jumps.push((self.lines.len(), addr, target_addr, 0xFFFF, entry.is_guessed()));
            }

            // Record calls
            if let Some(call_addr) = entry.call_target() {
                if call_addr >= 0x200 && call_addr <= MAX_PROGRAM_ADDRESS {
                    call_targets.insert(call_addr, entry.is_guessed());
                }
            }

            // Add line
            let next_addr = addr.saturating_add(entry.len());
            self.address_to_line.insert(addr, self.lines.len());
            self.lines.push((addr, entry));

            // Find nearest line to cursor
            let d = (addr as i32 - cursor as i32).abs();
            if d < min_distance {
                min_distance = d;
                line = self.lines.len();
            }
            addr = next_addr;
        }

        // Resolve pending jump target address
        for (addr, targets) in jump_targets {
            for i in targets {
                // Ignore jumps into raw data
                if let Some(line) = self.address_to_line.get(&addr) {
                    if let Some((_, to)) = self.lines.get(*line) {
                        if to.is_instruction() {
                            self.jumps[i].3 = *line;
                        }
                    }
                }
            }
        }

        // Detect function bodies
        for (start_addr, guessed) in call_targets {
            let start_line = *self.address_to_line.get(&start_addr).unwrap_or(&0xFFFF);
            if start_line != 0xFFFF {
                // Limit detection size for guessed instructions
                let mut end_addr = start_addr;
                let mut previous_skip = false;
                let mut has_return = false;
                let mut marker = vec![(start_line, 0)];
                while end_addr < start_addr.saturating_add(0x200).min(MAX_PROGRAM_ADDRESS) {
                    let entry_line = *self.address_to_line.get(&end_addr).unwrap_or(&0xFFFF);
                    let entry = self.address_entry(entries, end_addr);
                    if entry.is_return() {
                        if previous_skip {
                            marker.push((entry_line, 1));

                        } else {
                            marker.push((entry_line, 2));
                            has_return = true;
                            break;
                        }
                    }
                    end_addr = end_addr.saturating_add(entry.len());
                    previous_skip = entry.is_skip();
                }
                let end_line = *self.address_to_line.get(&end_addr).unwrap_or(&0xFFFF);
                if end_line != 0xFFFF {
                    if has_return || !guessed  {
                        // TODO ignore any overlapping bodies
                        self.calls.push((
                            start_line,
                            start_addr,
                            end_line,
                            end_addr,
                            guessed,
                            marker
                        ));
                    }
                }
            }
        }
        self.calls.sort();
        line
    }

    fn address_entry(&mut self, entries: &HashMap<MemoryAddress, Entry>, addr: MemoryAddress) -> AnalyzerEntry {
        if let Some(entry) = self.entries.get(&addr) {
            entry.clone()

        } else {
            let entry = self.decode(entries, addr);
            self.entries.insert(addr, entry.clone());
            entry
        }
    }

    fn analyze_jumps(
        &self,
        height: usize,
        line_offset: Option<usize>

    ) -> Vec<AnalyzerJump> {

        // Calculate jump lines
        let max_line = height as isize - 1;
        let line_offset = line_offset.unwrap_or(0);
        let mut jump_lines = Vec::new();
        for (from, from_addr, to_addr, to, guessed) in &self.jumps {

            // Skip likely invalid jumps
            if *to == 0xFFFF {
                continue;
            }

            // Offset so they match the assembly lines on screen
            let mut from = *from as isize;
            let mut to = *to as isize;

            from = from.saturating_sub(line_offset as isize);
            to = to.saturating_sub(line_offset as isize);
            if from >= 0 || to >= 0 {
                // Clip into line space
                let (from_origin, from) = if from >= 0 {
                    (from <= max_line, from.min(max_line))

                } else {
                    (false, from.max(0))
                };
                let (to_origin, to) = if to <= max_line {
                    (to > 0, to.max(0))

                } else {
                    (false, to.min(max_line))
                };
                // Order from top to bottom
                if from <= to {
                    jump_lines.push((from, from_origin, to, to_origin, *from_addr, *to_addr, from, *guessed));

                } else {
                    jump_lines.push((to, to_origin, from, from_origin, *from_addr, *to_addr, from, *guessed));
                }
            }
        }

        // Sort by length ascending
        jump_lines.sort_by(|a, b| {
            let d = (a.0 - a.2).abs() - (b.0 - b.2).abs();
            if d < 0 {
                Ordering::Less

            } else if d > 0 {
                Ordering::Greater

            } else {
                Ordering::Equal
            }
        });

        // Compact lines before drawing
        let mut jumps = Vec::new();
        let mut cells = HashSet::new();
        for (from, f, to, o, from_addr, to_addr, origin, guessed) in jump_lines {
            let mut indent = 0;
            'outer: while indent < 10 {
                for y in from..to {
                    let p = (indent, y);
                    if cells.contains(&p) {
                        indent += 1;
                        continue 'outer;
                    }
                }
                for y in from..to {
                    let p = (indent, y);
                    cells.insert(p);
                }
                break;
            }

            // Ignore jumps to same location
            // Only consider jumps
            // - either into / out of the view area
            // - or on a level lower than 5
            if from != to && ((f || o) || indent < 7) {
                jumps.push((from as usize, f, to as usize, o, indent, from_addr, to_addr, origin as usize, guessed));
            }
        }
        jumps
    }

    fn filter_calls(
        &self,
        height: usize,
        line_offset: Option<usize>

    ) -> Vec<AnalyzerCall> {
        let max_line = height as isize;
        let line_offset = line_offset.unwrap_or(0);
        let mut calls = Vec::with_capacity(self.calls.len());
        for (from, from_addr, to, to_addr, guessed, markers) in &self.calls {
            let from = (*from as isize).saturating_sub(line_offset as isize);
            let to = (*to as isize).saturating_sub(line_offset as isize);
            if from >= 0 || to >= 0 {
                // Clip lines into line space
                let from = if from >= 0 {
                    from.min(max_line)

                } else {
                    from.max(0)
                };
                let to = if to <= max_line {
                    to.max(0)

                } else {
                    to.min(max_line)
                };

                // Clip markers into line space
                let mut filter_markers = Vec::new();
                for (line, typ) in markers {
                    let line = (*line as isize).saturating_sub(line_offset as isize);
                    if line >= 0 && line <= max_line {
                        filter_markers.push((line as usize, *typ));
                    }
                }
                calls.push((from as usize, *from_addr, to as usize, *to_addr, *guessed, filter_markers));
            }
        }
        calls
    }

    fn decode(&self, entries: &HashMap<MemoryAddress, Entry>, addr: MemoryAddress) -> AnalyzerEntry {
        match entries.get(&addr) {
            Some(entry) => match entry {
                Entry::Instruction(i) => {
                    if i.is_long() {
                        // TODO get data for I load
                        AnalyzerEntry::InstructionLong(0x0000, false)

                    } else {
                        AnalyzerEntry::Instruction(*i, false)
                    }
                },
                Entry::Data(h) => {
                    // Guess instruction mnemonics in lower memory
                    // But don't guess bytes forcefully markes as data
                    if addr < 0x0EA0 && !self.overwrites.contains_key(&addr) {
                        if let Some(Entry::Data(l)) = entries.get(&(addr + 1)) {
                            let word = ((*h as MemoryAddress) << 8) | *l as MemoryAddress;
                            let i = Instruction::from_u16(word);
                            let mnemonic = i.mnemonic();
                            if mnemonic != "  ??" {
                                if i.is_long() {
                                    // TODO get data for I load
                                    return AnalyzerEntry::InstructionLong(0x0000, true);

                                } else {
                                    return AnalyzerEntry::Instruction(Instruction::from_u16(word), true);
                                }
                            }
                        }
                    }
                    AnalyzerEntry::DataByte(*h)
                },
                Entry::DataFromRegister(b, reg) => AnalyzerEntry::RegisterByte(*reg, *b)
            },
            None => AnalyzerEntry::None
        }
    }

}

