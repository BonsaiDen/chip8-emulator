// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::cell::RefCell;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use app::{Input, imgui, Window as WinitWindow};


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod analyzer;
mod logic;
use self::logic::{BPCondition, BPLocation, DebuggerLogic, Entry};
mod ui;
use self::ui::DebuggerUI;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::vm::Instruction;
use crate::memory::{MemoryAddress, Memory, Register};


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub type DebuggerCell = Rc<RefCell<Debugger>>;
pub struct Debugger {
    enabled: bool,
    logic: DebuggerLogic,
    ui: DebuggerUI
}

impl Debugger {
    pub fn new_cell() -> DebuggerCell {
        Rc::new(RefCell::new(Debugger {
            enabled: false,
            logic: DebuggerLogic::new(),
            ui: DebuggerUI::new()
        }))
    }

    pub fn is_enabled(&self) -> bool {
        self.enabled
    }

    pub fn is_paused(&self) -> bool {
        self.enabled && self.logic.is_paused()
    }

    pub fn enable(&mut self) {
        self.enabled = true;
    }

    pub fn show(&mut self) {
        self.ui.show()
    }

    pub fn pause<S: Into<String>>(&mut self, reason: S) {
        self.ui.pause(&mut self.logic, reason.into());
    }

    pub fn update(&mut self, window: &WinitWindow, input: &Input) {
        if self.enabled {
            self.ui.update(&mut self.logic, window, input);
        }
    }

    pub fn draw(&mut self, ui: &imgui::Ui, input: &Input, memory: &mut Memory) {
        if self.enabled {
            self.ui.draw(&mut self.logic, ui, input, memory);
        }
    }

    pub fn cycles(&mut self) -> u16 {
        self.logic.cycles()
    }

    pub fn reverts(&mut self) -> Option<usize> {
        self.logic.reverts()
    }

    pub fn should_reset(&mut self) -> bool {
        self.logic.should_reset()
    }

    pub fn instruction(&mut self, addr: MemoryAddress, i: Instruction) {
        if self.enabled {
            self.logic.analyzer.invalidate(addr);
            self.logic.entries.insert(addr, Entry::Instruction(i));
        }
    }

    pub fn write_pc(&mut self, addr: MemoryAddress) {
        if self.enabled {
            // TODO pc register breakpoints
            //self.check_breakpoint(BPLocation::Register(reg), BPCondition::Write(Some(value)));
            self.logic.set_pc(addr);
            self.ui.move_cursor(addr);
        }
    }

    pub fn write_memory(&mut self, addr: MemoryAddress, value: u8) {
        if self.enabled {
            self.ui.require_update();
            self.logic.analyzer.invalidate(addr);
            // TODO mark as AccessedData for writes after ROM loads so it doesn't get guess decoded
            // as an instruction
            self.logic.entries.insert(addr, Entry::Data(value));
            self.check_breakpoint(BPLocation::Memory(addr), BPCondition::Write(Some(value)));
        }
    }

    pub fn write_memory_with_register(&mut self, addr: MemoryAddress, reg: Register, value: u8) {
        if self.enabled {
            self.ui.require_update();
            self.logic.analyzer.invalidate(addr);
            self.logic.entries.insert(addr, Entry::DataFromRegister(value, reg));
            self.check_breakpoint(BPLocation::Memory(addr), BPCondition::Write(Some(value)));
        }
    }

    pub fn read_memory(&mut self, addr: MemoryAddress, value: u8) {
        if self.enabled {
            //self.ui.require_update();
            // TODO differntiate between PC reads and I reads and mark latter as AccessedData
            self.check_breakpoint(BPLocation::Memory(addr), BPCondition::Read(Some(value)));
        }
    }

    pub fn write_register(&mut self, reg: Register, value: u8) {
        if self.enabled {
            self.ui.require_update();
            self.check_breakpoint(BPLocation::Register(reg), BPCondition::Write(Some(value)));
        }
    }

    pub fn read_register(&mut self, reg: Register, value: u8) {
        if self.enabled {
            //self.ui.require_update();
            self.check_breakpoint(BPLocation::Register(reg), BPCondition::Read(Some(value)));
        }
    }

    pub fn write_i(&mut self, _i: MemoryAddress) {
        if self.enabled {
            self.ui.require_update();
            // TODO i register breakpoints
            //self.check_breakpoint(BPLocation::Register(reg), BPCondition::Write(Some(value)));
        }
    }

    pub fn read_i(&mut self, _i: MemoryAddress) {
        if self.enabled {
            //self.ui.updated = true;
            // TODO i register breakpoints
            //self.check_breakpoint(BPLocation::Register(reg), BPCondition::Read(Some(value)));
        }
    }

    pub fn write_display(&mut self, addr: MemoryAddress, value: u8) {
        if self.enabled {
            //self.ui.require_update();
            self.check_breakpoint(BPLocation::Display(addr), BPCondition::Write(Some(value)));
        }
    }

    pub fn read_display(&mut self, addr: MemoryAddress, value: u8) {
        if self.enabled {
            //self.ui.require_update();
            self.check_breakpoint(BPLocation::Display(addr), BPCondition::Read(Some(value)));
        }
    }


    fn check_breakpoint(&mut self, location: BPLocation, condition: BPCondition) {
        if let Some(location) = self.logic.check_breakpoint(location, condition) {
            self.pause("Hit breakpoint".to_string());
            if let BPLocation::Memory(addr) = location {
                self.ui.move_cursor(addr);
            }
        }
    }

}

