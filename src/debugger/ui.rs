// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use app::{
    gamma_to_linear,
    Input,
    Key,
    imgui::{self, im_str, ChildWindow, Condition, MouseButton, TabBar, TabItem, StyleVar, StyleColor, Window},
    Window as WinitWindow
};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::debugger::DebuggerLogic;
use crate::emulator::EmulatorWindow;
use crate::memory::{MemoryAddress, Memory, Register};
use crate::debugger::analyzer::{AnalyzerCall, AnalyzerEntry, AnalyzerJump};


// Macros ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
macro_rules! eregister {
    ($s:expr, $memory:expr, $ui:expr, $name:expr, $get:ident, $set:ident) => {
        if reg($ui, im_str!("{} {:0>4X}", $name, $memory.$get())) {
            $s.open_modal(
                $ui, "Edit Register", $name, $memory.$get() as i32, &|_, m, v| {
                    m.$set(v)
                }
            );
        }
    }
}
macro_rules! rregister {
    ($s:expr, $memory:expr, $ui:expr, $name:expr, $r:expr) => {
        if reg($ui, im_str!("{} {:0>2X}", $name, $memory.reg($r))) {
            $s.open_modal(
                $ui, "Edit Register", $name, $memory.reg($r) as i32, &|_, m, v| {
                    m.set_reg($r, v)
                }
            );
        }
    }
}


// Enums ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
enum Text {
    Plain(String),
    Invert(bool),
    Color(Option<[f32; 4]>)
}


// Debugger UI ----------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DebuggerUI {
    visible: bool,
    cursor: MemoryAddress,
    memory_line: (usize, usize),
    memory_line_max: usize,
    lines: Vec<(bool, Vec<Text>)>,
    jumps: Vec<AnalyzerJump>,
    calls: Vec<AnalyzerCall>,
    modal_input: i32,
    modal_opened: bool,
    modal_callback: Option<&'static dyn Fn(&mut DebuggerUI, &mut Memory, i32)>,
    modal_field: &'static str,
    reason: Option<String>,
    updated: bool
}

impl DebuggerUI {
    pub fn new() -> Self {
        Self {
            visible: false,
            cursor: 0,
            memory_line: (0, 0),
            memory_line_max: 0,
            lines: Vec::new(),
            jumps: Vec::new(),
            calls: Vec::new(),
            modal_input: 0,
            modal_opened: false,
            modal_callback: None,
            modal_field: "",
            reason: None,
            updated: false,
        }
    }

    pub fn show(&mut self) {
        self.visible = true;
    }

    pub fn pause<S: Into<String>>(&mut self, logic: &mut DebuggerLogic, reason: S) {
        logic.pause();
        self.reason = Some(reason.into());
        self.visible = true;
        self.updated = true;
    }

    fn resume(&mut self, logic: &mut DebuggerLogic) {
        self.reason = None;
        logic.resume();
    }

    pub fn update(&mut self, logic: &mut DebuggerLogic, window: &WinitWindow, input: &Input) {

        // Toggle Debugger Visibility
        if input.key_pressed(Key::Tab) {
            self.visible = !self.visible;
            if self.visible {
                if window.scale() < 2 {
                    window.set_scale(2);
                }
            }
        }

        // Controls
        if logic.is_paused() {
            if input.key_pressed(Key::D) {
                logic.analyzer.mark_as_data(self.cursor);
                self.require_update();
            }
            if input.key_pressed(Key::P) {
                self.resume(logic);
            }
            if input.key_pressed(Key::N) {
                logic.step();
            }
            if input.key_pressed(Key::B) {
                logic.revert();
            }
            if input.key_pressed(Key::Down) {
                self.move_cursor(self.cursor.saturating_add(logic.analyzer.get_size(self.cursor)));
            }
            if input.key_pressed(Key::Up) {
                self.move_cursor(self.cursor.saturating_sub(logic.analyzer.get_size(self.cursor.saturating_sub(1))));
            }

            // Goto Jump Target
            if input.key_pressed(Key::F) {
                if let Some(target) = logic.analyzer.get_target(self.cursor) {
                    self.move_cursor(target);
                }
            }

        } else if input.key_pressed(Key::P) {
            self.pause(logic, "Paused by user");
        }

        if self.updated && logic.is_paused() {
            self.cursor = logic.analyzer.snap_address(self.cursor);
            self.refresh(logic);
            self.updated = false;
        }

    }

    pub fn draw(&mut self, logic: &mut DebuggerLogic, ui: &imgui::Ui, input: &Input, memory: &mut Memory) {
        if self.visible {
            self.draw_window(logic, ui, input, memory);
        }
    }

    pub fn require_update(&mut self) {
        self.updated = true;
    }

    pub fn move_cursor(&mut self, cursor: MemoryAddress) {
        self.cursor = cursor;
        self.updated = true;
    }

    pub fn open_modal(
        &mut self,
        ui: &imgui::Ui,
        t: &str,
        field: &'static str,
        v: i32,
        callback: &'static dyn Fn(&mut DebuggerUI, &mut Memory, i32)
    ) {
        self.modal_opened = true;
        self.modal_input = v;
        self.modal_callback = Some(callback);
        self.modal_field = field;
        ui.open_popup(&im_str!("{}", t));
    }

    pub fn modal(&mut self, memory: &mut Memory, ui: &imgui::Ui, title: &str) -> Option<i32> {
        ui.popup_modal(&im_str!("{}", title))
            .always_auto_resize(true)
            .resizable(false)
            .build(|| {
                imgui::InputInt::new(ui, &im_str!("{}", self.modal_field), &mut self.modal_input)
                    .auto_select_all(true)
                    .enter_returns_true(true)
                    .chars_decimal(false)
                    .chars_hexadecimal(true)
                    .build();

                if self.modal_opened {
                    ui.set_keyboard_focus_here(imgui::FocusedWidget::Previous);
                    self.modal_opened = false;
                }

                if ui.button(im_str!("OK"), [0.0, 0.0]) || ui.is_key_pressed(ui.key_index(imgui::Key::Enter)) {
                    if let Some(callback) = self.modal_callback.take() {
                        callback(self, memory, self.modal_input);
                    }
                    ui.close_current_popup();

                } else if ui.is_key_pressed(ui.key_index(imgui::Key::Escape)) {
                    ui.close_current_popup();
                }
            });
        None
    }

}

impl DebuggerUI {
    fn refresh(&mut self, logic: &mut DebuggerLogic) {
        // TODO Breakpoints
        // TODO mark function boundaries by finding CALLs, their targets and returns
        let pointer = logic.pc();
        let (line_count, lines, jumps, calls) = logic.analyzer.update(&logic.entries, self.cursor, 27);
        self.jumps = jumps;
        self.calls = calls;
        self.lines = lines.into_iter().map(|(addr, data)| {
            let mut content = Vec::new();
            let guessed = match data {
                AnalyzerEntry::Instruction(i, guessed) => {
                    let (_, nnn, nn, n, x, y) = i.decode();
                    let mnemonic = i.mnemonic();
                    let mnemonic = mnemonic.replace("Vx", &format!("{:?}", x));
                    let mnemonic = mnemonic.replace("Vy", &format!("{:?}", y));
                    let mnemonic = mnemonic.replace("NNN", &format!("{:0>4X?}", nnn));
                    let mnemonic = mnemonic.replace("NN", &format!("{:0>2X?}", nn));
                    let mnemonic = mnemonic.replace("ZZ", &format!("{:0>1X?}", n));
                    // TODO binary formatting
                    let mnemonic = mnemonic.replace("BB", &format!("{:0>1X?}", (nnn >> 8) as u8 & 0x03));
                    content.push(Text::Plain(format!("{:0>2X} {:0>2X}             {: <20}", i.0 >> 8, i.0 & 0xFF, mnemonic)));
                    guessed
                },
                AnalyzerEntry::InstructionLong(word, guessed) => {
                    content.push(Text::Plain(format!("F0 00 {:0>4X}          LD I, {:0>4X}       ", word, word)));
                    guessed
                },
                AnalyzerEntry::DataByte(byte) => {
                    content.push(Text::Plain(format!("-- {:0>2X}", byte)));
                    false
                },
                AnalyzerEntry::RegisterByte(reg, byte) => {
                    content.push(Text::Invert(true));
                    content.push(Text::Plain(format!("{:?}", reg)));
                    content.push(Text::Invert(false));
                    content.push(Text::Plain(format!(" {:0>2X}", byte)));
                    false
                },
                AnalyzerEntry::None => {
                    content.push(Text::Plain(format!("??")));
                    false
                }
            };
            let mut text = Vec::new();
            // TODO check for breakpoint and mark
            // TODO need to draw block via rect instead of unicode
            //if line.breakpoint {
            //    text.push(Text::Color(Some([1.0, 0.0, 0.0, 1.0])));
            //    text.push(Text::Plain("■".to_string()));
            //    text.push(Text::Color(None));

            //} else {
                text.push(Text::Plain(" ".to_string()));
            //}
            text.push(Text::Plain("               ".to_string()));

            if addr == pointer {
                text.push(Text::Invert(true));

            } else if guessed {
                text.push(Text::Color(Some([0.5, 0.5, 0.5, 1.0])));
            }
            text.push(Text::Plain(format!("0x{:0>4X}    ", addr)));
            text.append(&mut content);
            (self.cursor == addr, text)

        }).collect();
        self.memory_line.0 = self.cursor as usize;
        self.memory_line_max = line_count;
    }

    fn draw_window(&mut self, logic: &mut DebuggerLogic, ui: &imgui::Ui, input: &Input, memory: &mut Memory) {
        Window::new(im_str!("Debugger"))
            .resizable(false)
            .position([2.0, 2.0], Condition::FirstUseEver)
            .size([640.0, 508.0], Condition::FirstUseEver)
            .build(ui, || {
                Self::draw_controls(self, logic, ui);
                ui.separator();
                TabBar::new(im_str!("tabs"))
                    .build(ui, || {
                        let color = ui.style_color(StyleColor::Tab);
                        let c = ui.push_style_color(StyleColor::Tab, [
                            color[0] * 0.2,
                            color[1] * 0.2,
                            color[2] * 0.2,
                            1.0
                        ]);

                        // TODO how to select default tab?
                        TabItem::new(im_str!("Memory"))
                            .build(ui, || self.draw_memory_tab(logic, ui, input, memory));

                        TabItem::new(im_str!("Breakpoints"))
                            .build(ui, || Self::draw_breakpoint_tab(logic, ui));

                        TabItem::new(im_str!("Keyboard"))
                            .build(ui, || Self::draw_keyboard_tab(logic, ui));

                        TabItem::new(im_str!("Display"))
                            .build(ui, || {
                                ui.text(im_str!("TODO"));
                            });

                        c.pop(ui);

                    });

                ui.set_cursor_pos([0.0, ui.window_size()[1] - 32.0]);
                ui.separator();
                Self::draw_status(self, ui);
            });
    }

    fn draw_controls(self: &mut DebuggerUI, logic: &mut DebuggerLogic, ui: &imgui::Ui) {
        if logic.is_paused() {
            if ui.button(im_str!("Resume"), [50.0, 20.0]) {
                self.resume(logic);
            }
            ui.same_line(0.0);
            if ui.button(im_str!("Next"), [50.0, 20.0]) {
                logic.step();
            }

        } else {
            if ui.button(im_str!("Pause"), [50.0, 20.0]) {
                self.pause(logic, "Paused by user");
            }
        }
        ui.same_line(0.0);

        let p = ui.cursor_pos();
        let w = ui.window_content_region_max()[0];
        ui.set_cursor_pos([w - 50.0, p[1]]);
        if ui.button(im_str!("Reset"), [50.0, 20.0]) {
            logic.reset();
        }
    }

    fn draw_status(self: &mut DebuggerUI, ui: &imgui::Ui) {
        let status = if let Some(r) = self.reason.as_ref() {
            r.to_string()

        } else {
            "Running".to_string()
        };
        ui.text(im_str!("Status: {}", status));
        ui.same_line(640.0 - 120.00);
        ui.button(im_str!("Add Breakpoint"), [115.0, 0.0]);
    }

    fn draw_memory_tab(&mut self, logic: &mut DebuggerLogic, ui: &imgui::Ui, input: &Input, memory: &mut Memory) {
        // Details
        ChildWindow::new(im_str!("registers"))
            .size([70.0, 330.0])
            .build(ui, || Self::draw_memory_registers(self, ui, memory));

        ui.same_line(0.0);

        ChildWindow::new(im_str!("code"))
            .size([460.0, 410.0])
            .build(ui, || {
                ui.text(im_str!("                Addr      Data              Mnemonic"));
                ui.separator();
                let mut line = self.memory_line;
                let lines = &self.lines;
                let jumps = &self.jumps;
                let calls = &self.calls;
                let pc = logic.pc();
                let scrolled = line_scroller(ui, 460.0, 28, self.memory_line_max, &mut line, |ui| {
                    draw_text_lines(ui, 446.0, lines);
                    draw_jump_lines(ui, jumps, pc);
                    draw_call_lines(ui, calls, pc);
                });
                self.memory_line = line;
                if scrolled {
                    self.cursor = self.memory_line.0 as MemoryAddress;
                    self.updated = true;
                }
            });

        ui.same_line(0.0);
        ChildWindow::new(im_str!("stack"))
            .size([98.0, 200.0])
            .build(ui, || {
                ui.text(im_str!("Stack"));
                ui.separator();
                if memory.stack().is_empty() {
                    ui.text("Empty");

                } else {
                    for (i, addr) in memory.stack().iter().enumerate() {
                        ui.text(im_str!("{}: {:0>4X}", i, addr));
                    }
                }
            });

        // Goto Address Dialog
        if input.key_pressed(Key::G) {
            self.open_modal(ui, "Goto Address", "PC", self.cursor as i32, &|ui, _, v| {
                ui.move_cursor(v.min(0xFFFF).max(0) as u16);
            });
        }
        self.modal(memory, ui, "Goto Address");
    }

    fn draw_memory_registers(self: &mut DebuggerUI, ui: &imgui::Ui, memory: &mut Memory) {
        fn reg(ui: &imgui::Ui, s: imgui::ImString) -> bool {
            ui.text(s);
            let a = ui.item_rect_min();
            let b = ui.item_rect_max();
            if ui.is_mouse_hovering_rect(a, b) {
                let draw_list = ui.get_window_draw_list();
                let mut c = ui.style_color(StyleColor::ButtonActive);
                c[3] = 0.20;
                draw_list.add_rect_filled_multicolor(a, b, c, c, c, c);
                ui.is_mouse_clicked(MouseButton::Left)

            } else {
                false
            }
        }

        ui.text(im_str!("Registers"));
        ui.separator();
        let spacing = ui.push_style_var(StyleVar::ItemSpacing([0.0, 0.0]));

        // Registers
        eregister!(self, memory, ui, "PC", pc, set_pc);
        eregister!(self, memory, ui, " I", i, set_i);
        ui.text(im_str!(""));
        eregister!(self, memory, ui, "DT", dt, set_dt);
        eregister!(self, memory, ui, "DS", ds, set_ds);
        ui.text(im_str!(""));
        rregister!(self, memory, ui, "V0", Register::V0);
        rregister!(self, memory, ui, "V1", Register::V1);
        rregister!(self, memory, ui, "V2", Register::V2);
        rregister!(self, memory, ui, "V3", Register::V3);
        rregister!(self, memory, ui, "V4", Register::V4);
        rregister!(self, memory, ui, "V5", Register::V5);
        rregister!(self, memory, ui, "V6", Register::V6);
        rregister!(self, memory, ui, "V7", Register::V7);
        rregister!(self, memory, ui, "V8", Register::V8);
        rregister!(self, memory, ui, "V9", Register::V9);
        rregister!(self, memory, ui, "VA", Register::VA);
        rregister!(self, memory, ui, "VB", Register::VB);
        rregister!(self, memory, ui, "VC", Register::VC);
        rregister!(self, memory, ui, "VD", Register::VD);
        rregister!(self, memory, ui, "VE", Register::VE);
        rregister!(self, memory, ui, "VF", Register::VF);

        spacing.pop(ui);
        self.modal(memory, ui, "Edit Register");
    }

    fn draw_breakpoint_tab(_: &mut DebuggerLogic, ui: &imgui::Ui) {
        let unit = ui.calc_text_size(im_str!(" "), false, 0.0)[0];
        ui.text(im_str!("Active     Register     Address      Read         Write       Actions"));
        ui.separator();

        ChildWindow::new(im_str!("code"))
            .size([628.0, 374.0])
            .build(ui, || {
                for _ in 0..25 {
                    let mut active = false;
                    ui.checkbox(im_str!(""), &mut active);
                    ui.same_line(unit * 11.0);
                    ui.text(im_str!("--"));
                    ui.same_line(unit * 24.0);
                    ui.text(im_str!("--"));
                    ui.same_line(unit * 37.0);
                    ui.text(im_str!("--"));
                    ui.same_line(unit * 51.0);
                    ui.text(im_str!("--"));
                    ui.same_line(640.0 - 50.0);
                    ui.button(im_str!("X"), [20.0, 0.0]);
                    ui.same_line(640.0 - 75.0);
                    ui.button(im_str!("E"), [20.0, 0.0]);
                }
            });
    }

    fn draw_keyboard_tab(_: &mut DebuggerLogic, ui: &imgui::Ui) {

        let mut toggle = false;
        ui.checkbox(im_str!("Toggle Mode"), &mut toggle);

        // TODO change StyleColor::ButtonActive when in toggle mode

        // TODO read key states from memory / write them to memory
        ui.text(im_str!("TODO"));
        ui.button(im_str!("1"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("2"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("3"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("A"), [40.0, 40.0]);

        ui.button(im_str!("4"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("5"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("6"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("B"), [40.0, 40.0]);

        ui.button(im_str!("7"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("8"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("9"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("C"), [40.0, 40.0]);

        ui.button(im_str!("*"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("0"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("#"), [40.0, 40.0]);
        ui.same_line(0.0);
        ui.button(im_str!("D"), [40.0, 40.0]);

    }

}


// Helpers --------------------------------------------------------------------
fn line_scroller<T: FnMut(&imgui::Ui)>(
    ui: &imgui::Ui,
    width: f32,
    visible_lines: usize,
    total_lines: usize,
    line: &mut (usize, usize),
    mut render_lines: T

) -> bool {
    let spacing = ui.push_style_var(imgui::StyleVar::ItemSpacing([0.0, 0.0]));
    let lh = ui.text_line_height_with_spacing();
    spacing.pop(ui);
    let height = visible_lines as f32 * lh;

    // Line Window
    let overlay = ui.cursor_pos();
    ChildWindow::new(im_str!("line_scroller_window"))
        .size([width, height])
        .build(ui, || {
            ui.set_cursor_pos([0.0, 0.0]);
            render_lines(ui)
        });

    // Custom Scroll Bar Emulation
    ui.set_cursor_pos(overlay);

    let mut scrolled = false;
    ChildWindow::new(im_str!("line_scroller_bar"))
        .size([width, height])
        .always_vertical_scrollbar(true)
        .build(ui, || {

            // Force scroll area
            ChildWindow::new(im_str!("line_scroller_padding"))
                .size([1.0, total_lines as f32* lh])
                .draw_background(false)
                .build(ui, || {});

            // Keyboard Controls
            if ui.is_key_pressed(ui.key_index(imgui::Key::Home)) {
                ui.set_scroll_y(0.0);

            } else if ui.is_key_pressed(ui.key_index(imgui::Key::End)) {
                ui.set_scroll_y(total_lines as f32 * lh);

            } else if ui.is_key_pressed(ui.key_index(imgui::Key::PageUp)) {
                ui.set_scroll_y(line.0.saturating_sub(visible_lines) as f32 * lh);

            } else if ui.is_key_pressed(ui.key_index(imgui::Key::PageDown)) {
                ui.set_scroll_y(line.0.saturating_add(visible_lines) as f32 * lh);
            }

            // Line changed on the outside
            if line.0 != line.1 {
                ui.set_scroll_y(line.0 as f32 * lh);
                line.1 = line.0;

            } else {
                // Snap scrolling to lines
                let raw_line = ui.scroll_y() / lh;
                let index_line = raw_line.round();
                if index_line != raw_line {
                    ui.set_scroll_y(index_line * lh);
                }

                // Update outside line when changed by scrolling
                let index_line = index_line as usize;
                if line.0 != index_line {
                    line.0 = index_line;
                    line.1 = index_line;
                    scrolled = true;
                }
            }

        });

    scrolled
}

fn draw_text_lines(ui: &imgui::Ui, width: f32, lines: &[(bool, Vec<Text>)]) {
    let spacing = ui.push_style_var(StyleVar::ItemSpacing([0.0, 0.0]));
    let lh = ui.text_line_height_with_spacing();
    spacing.pop(ui);

    let draw_list = ui.get_window_draw_list();
    let unit = ui.calc_text_size(im_str!(" "), false, 0.0)[0];
    let [ox, mut y] = ui.window_pos();

    draw_list.channels_split(3, |channels| {
        for (highlight, tokens) in lines {
            let mut x = ox;
            let mut text_color = vec![[1.0, 1.0, 1.0, 1.0]];
            let mut inverted = false;
            for t in tokens {
                match t {
                    Text::Plain(t) => {
                        let w = t.len() as f32 * unit;
                        let col = if inverted {
                            invert_color(*text_color.last().unwrap())

                        } else {
                            *text_color.last().unwrap()
                        };
                        let a = [x, y];
                        let b = [(x + w).min(ox + width), y + lh];

                        // Background
                        if inverted {
                            let bg = gamma_to_linear(invert_color(col));
                            channels.set_current(1);
                            draw_list.add_rect_filled_multicolor(a, b, bg, bg, bg, bg);
                            draw_list.add_text([x, y], col, t);
                        }

                        // Text
                        let col = gamma_to_linear(col);
                        channels.set_current(2);
                        draw_list.add_text([x, y], col, t);
                        x += w;
                    },
                    Text::Color(Some(v)) => {
                        text_color.push(*v);
                    },
                    Text::Color(None) => if text_color.len() > 1 {
                        text_color.pop();
                    },
                    Text::Invert(i) => {
                        inverted = *i;
                    }
                }
            }
            if *highlight {
                channels.set_current(0);
                let hi = ui.style_color(StyleColor::Header);
                draw_list.add_rect_filled_multicolor([ox, y], [ox + width, y + lh], hi, hi, hi, hi);
            }
            y += lh;
        }
    });
}

fn draw_jump_lines(ui: &imgui::Ui, jumps: &[AnalyzerJump], instruction_pointer: MemoryAddress) {
    let spacing = ui.push_style_var(StyleVar::ItemSpacing([0.0, 0.0]));
    let lh = ui.text_line_height_with_spacing();
    spacing.pop(ui);

    let draw_list = ui.get_window_draw_list();
    let unit = ui.calc_text_size(im_str!(" "), false, 0.0)[0];
    let [ox, oy] = ui.window_pos();
    let ox = ox + unit * 14.0 + 0.5;
    for (from, from_origin, to, to_target, indent, from_addr, to_addr, from_line, guessed) in jumps {

        let color = if *from_addr == instruction_pointer {
            gamma_to_linear([0.0, 1.0, 0.0, 1.0])

        } else if *guessed {
            gamma_to_linear([0.5, 0.5, 0.5, 1.0])

        } else if *to_addr == instruction_pointer {
            gamma_to_linear([1.0, 0.0, 0.0, 1.0])

        } else {
            gamma_to_linear([1.0, 1.0, 1.0, 1.0])
        };
        let depth = *indent as f32 * unit;
        let x = ox - depth;

        // Markers
        for i in *from..=*to {
            let y = oy + lh * i as f32;
            let m = if i == *from_line { "o" } else { ">" };
            if i == *from && *from_origin {
                draw_list.add_line(
                    [x, oy + *from as f32 * lh + lh * 0.5],
                    [x + depth + unit, oy + *from as f32 * lh + lh * 0.5],
                    color

                ).build();
                draw_list.add_text([ox + unit, y], color, im_str!("{}", m));

            } else if i == *to && *to_target {
                draw_list.add_line(
                    [x, oy + *to as f32 * lh + lh * 0.5],
                    [x + depth + unit, oy + *to as f32 * lh + lh * 0.5],
                    color

                ).build();
                draw_list.add_text([ox + unit, y], color, im_str!("{}", m));
            }
        }

        // Vertical line
        let f = if *from_origin { lh * 0.5 } else { 0.0 };
        let t = if *to_target { -lh * 0.5 } else { 0.0 };
        draw_list.add_line(
            [x, oy + *from as f32 * lh + f],
            [x, oy + (*to as f32 + 1.0) * lh + t],
            color

        ).build();

    }
}

fn draw_call_lines(ui: &imgui::Ui, calls: &[AnalyzerCall], instruction_pointer: MemoryAddress) {
    let spacing = ui.push_style_var(StyleVar::ItemSpacing([0.0, 0.0]));
    let lh = ui.text_line_height_with_spacing();
    spacing.pop(ui);

    let draw_list = ui.get_window_draw_list();
    let unit = ui.calc_text_size(im_str!(" "), false, 0.0)[0];
    let [ox, oy] = ui.window_pos();
    let ox = ox + unit * 24.0 + 0.5;
    for (from, from_addr, to, to_addr, guessed, markers) in calls {
        let color = if instruction_pointer == *to_addr {
            gamma_to_linear([1.0, 0.0, 0.0, 1.0])

        } else if instruction_pointer >= *from_addr && instruction_pointer < *to_addr {
            gamma_to_linear([0.0, 1.0, 0.0, 1.0])

        } else if *guessed {
            gamma_to_linear([0.5, 0.5, 0.5, 1.0])

        } else {
            gamma_to_linear([1.0, 1.0, 1.0, 1.0])
        };
        draw_list.add_line(
            [ox, oy + *from as f32 * lh + lh * 0.5],
            [ox, oy + (*to as f32 + 1.0) * lh - lh * 0.5],
            color

        ).build();
        for (line, t) in markers {
            let (x, y) = match t {
                0 => (
                    [ox - unit, oy + *from as f32 * lh + lh * 0.5],
                    [ox + unit, oy + *from as f32 * lh + lh * 0.5]
                ),
                2 => (
                    [ox, oy + *line as f32 * lh + lh * 0.5],
                    [ox + unit, oy + *line as f32 * lh + lh * 0.5]
                ),
                _ => (
                    [ox, oy + *line as f32 * lh + lh * 0.5],
                    [ox + unit, oy + *line as f32 * lh + lh * 0.5]
                )
            };
            draw_list.add_line(x, y, color).build();
        }
    }
}

fn invert_color(c: [f32; 4]) -> [f32; 4] {
    [1.0 - c[0], 1.0 - c[1], 1.0 - c[2], 1.0]
}

