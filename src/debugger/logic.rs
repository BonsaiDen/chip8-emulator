// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::collections::HashMap;


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
use super::analyzer::Analyzer;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::vm::Instruction;
use crate::memory::{MemoryAddress, Register};


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
// TODO support Instruction breakpoints with value / bitmask / register values
#[derive(Debug, Hash, Eq, PartialEq)]
pub enum BPLocation {
    Register(Register),
    Display(MemoryAddress),
    Memory(MemoryAddress)
}

#[derive(Debug, Hash, Eq, PartialEq)]
pub enum BPCondition {
    Write(Option<u8>),
    Read(Option<u8>)
}

pub enum Entry {
    Instruction(Instruction),
    Data(u8),
    DataFromRegister(u8, Register)
}


// Debugger Logic -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DebuggerLogic {
    pub analyzer: Analyzer,
    instruction_pointer: MemoryAddress,
    pub entries: HashMap<MemoryAddress, Entry>,
    breakpoint_map: HashMap<BPLocation, usize>,
    breakpoints: Vec<(bool, BPCondition)>,
    should_reset: bool,
    cycles: Option<u16>,
    reverts: Option<usize>,
    paused: bool
}

impl DebuggerLogic {
    pub fn new() -> Self {
        Self {
            analyzer: Analyzer::new(),
            instruction_pointer: 0,
            entries: HashMap::with_capacity(4096),
            breakpoint_map: HashMap::new(),
            breakpoints: Vec::new(),
            should_reset: false,
            cycles: None,
            reverts: None,
            paused: false
        }
    }

    pub fn pc(&self) -> MemoryAddress {
        self.instruction_pointer
    }

    pub fn set_pc(&mut self, addr: MemoryAddress) {
        self.instruction_pointer = addr;
    }

    pub fn should_reset(&mut self) -> bool {
        let r = self.should_reset;
        self.should_reset = false;
        r
    }

    pub fn cycles(&mut self) -> u16 {
        self.cycles.take().unwrap_or(0)
    }

    pub fn reverts(&mut self) -> Option<usize> {
        self.reverts.take()
    }

    pub fn reset(&mut self) {
        self.should_reset = true;
    }

    pub fn step(&mut self) {
        self.cycles = Some(1);
    }

    pub fn revert(&mut self) {
        self.reverts = Some(1);
    }

    pub fn pause(&mut self) {
        self.paused = true;
    }

    pub fn is_paused(&self) -> bool {
        self.paused
    }

    pub fn resume(&mut self) {
        self.paused = false;
    }

    pub fn check_breakpoint(&mut self, location: BPLocation, condition: BPCondition) -> Option<BPLocation> {
        if let Some(index) = self.breakpoint_map.get(&location) {
            let b = self.breakpoints.get(*index).expect("Missing breakpoint data");
            let hit = match (b, condition) {
                ((true, BPCondition::Write(None)), BPCondition::Write(_)) => true,
                ((true, BPCondition::Write(Some(a))), BPCondition::Write(Some(b))) => *a == b,
                ((true, BPCondition::Read(None)), BPCondition::Read(_)) => true,
                ((true, BPCondition::Read(Some(a))), BPCondition::Read(Some(b))) => *a == b,
                _ => false
            };
            if hit {
                return Some(location);
            }
        }
        None
    }

}

