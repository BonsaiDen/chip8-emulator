// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::collections::VecDeque;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use rand::prelude::*;
use enum_primitive_derive::Primitive;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::vm::Instruction;
use crate::debugger::DebuggerCell;
use crate::display::DISPLAY_BUFFER_SIZE;


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub const MAX_MEMORY_STATES: usize = 256;
pub const MAX_PROGRAM_ADDRESS: u16 = 0x0EA0;
pub const MAX_DATA_ADDRESS: u16 = 0xF000;


// Structs / Enums ------------------------------------------------------------
// ----------------------------------------------------------------------------
#[allow(non_camel_case_types)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Primitive)]
pub enum Register {
    V0 = 0,
    V1 = 1,
    V2 = 2,
    V3 = 3,
    V4 = 4,
    V5 = 5,
    V6 = 6,
    V7 = 7,
    V8 = 8,
    V9 = 9,
    VA = 10,
    VB = 11,
    VC = 12,
    VD = 13,
    VE = 14,
    VF = 15
}

#[derive(Debug, Default, Copy, Clone)]
pub struct RegisterBank(pub [u8; 16]);

impl std::ops::Index<Register> for RegisterBank {
    type Output = u8;
    fn index(&self, r: Register) -> &Self::Output {
        &self.0[r as usize]
    }
}
impl std::ops::IndexMut<Register> for RegisterBank {
    fn index_mut(&mut self, r: Register) -> &mut Self::Output {
        &mut self.0[r as usize]
    }
}

pub type MemoryAddress = u16;

#[derive(Debug)]
pub struct MemoryError {
    pub pc: MemoryAddress,
    pub reason: String,
    pub instruction: Option<Instruction>,
    pub i: MemoryAddress
}

#[derive(Clone)]
struct MemoryData {
    i: MemoryAddress,
    pc: MemoryAddress,
    ds: u8,
    dt: u8,
    registers: RegisterBank,
    stack: Vec<MemoryAddress>,
    keys: [bool; 16],
    key: Option<u8>,
    bytes: Vec<u8>,
    pixels: Vec<u8>
}

impl MemoryData {
    fn new() -> Self {
        Self {
            i: 0,
            pc: 0x200,
            ds: 0,
            dt: 0,
            registers: RegisterBank::default(),
            stack: Vec::new(),
            bytes: vec![0; 0xFFFF],
            pixels: vec![0; DISPLAY_BUFFER_SIZE],
            keys: [false; 16],
            key: None
        }
    }
}


// Memory Abstraction ---------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Memory {
    data: MemoryData,
    states: VecDeque<MemoryData>,
    debugger: DebuggerCell,
}

impl Memory {
    pub fn new(debugger: DebuggerCell) -> Self {
        Self {
            data: MemoryData::new(),
            states: VecDeque::new(),
            debugger
        }
    }


    // Keyboard ---------------------------------------------------------------
    pub fn read_any_key(&mut self) -> Option<u8> {
        self.data.key.take()
    }

    pub fn read_key(&mut self, key: u8) -> bool {
        self.data.keys[key as usize]
    }

    pub fn write_key(&mut self, key: u8, down: bool) {
        self.data.keys[key as usize] = down;
        if down {
            self.data.key = Some(key);
        }
    }


    // VM / System ------------------------------------------------------------
    pub fn load_rom(&mut self, buffer: &[u8]) {
        // TODO handle cases where ROM is too big
        for i in 0..buffer.len() {
            self.write(i as MemoryAddress + 0x200, buffer[i]).expect("rom setup failed");
        }
        self.states.clear();
        self.states.push_back(self.data.clone());
    }

    pub fn reset(&mut self) {
        self.data = MemoryData::new();
        self.write_pc(self.data.pc).ok();
        self.states.clear();
        self.states.push_back(self.data.clone());
    }

    pub fn revert(&mut self) {
        if let Some(data) = self.states.pop_back() {
            self.data = data;
            self.write_pc(self.data.pc).ok();
        }
    }

    pub fn set_flag_register(&mut self, v: u8) {
        self.write_register(Register::VF, v);
    }

    pub fn read_u16(&mut self) -> Result<u16, MemoryError> {
        let h = self.read(self.data.pc)? as u16;
        let l = self.read(self.data.pc.wrapping_add(1))? as u16;
        self.data.pc = self.data.pc.wrapping_add(2);
        self.debugger.borrow_mut().write_pc(self.data.pc);
        Ok(h << 8 | l)
    }

    pub fn read_instruction(&mut self) -> Result<Instruction, MemoryError> {

        // Store memory state for debugger rewinding
        if self.debugger.borrow().is_paused() {
            if self.states.len() > MAX_MEMORY_STATES {
                self.states.pop_front();
            }
            self.states.push_back(self.data.clone());
        }

        let pc = self.data.pc;
        let i = Instruction::from_u16(self.read_u16()?);
        self.debugger.borrow_mut().instruction(pc, i);
        Ok(i)
    }

    pub fn skip_instruction(&mut self) -> Result<(), MemoryError> {
        let n = self.read_u16()?;
        // XO - skip over F000 NNNN instruction.
        if n == 0xF000 {
            self.read_u16()?;
        }
        Ok(())
    }

    pub fn write(&mut self, addr: MemoryAddress, value: u8) -> Result<(), MemoryError> {
        self.in_address_range(addr)?;
        self.data.bytes[addr as usize] = value;
        self.debugger.borrow_mut().write_memory(addr, value);
        Ok(())
    }

    pub fn write_with_register(&mut self, addr: MemoryAddress, reg: Register, value: u8) -> Result<(), MemoryError> {
        self.write(addr, value)?;
        self.debugger.borrow_mut().write_memory_with_register(addr, reg, value);
        Ok(())
    }

    pub fn read(&self, addr: MemoryAddress) -> Result<u8, MemoryError> {
        self.in_address_range(addr)?;
        let value = self.data.bytes[addr as usize];
        self.debugger.borrow_mut().read_memory(addr, value);
        Ok(value)
    }

    pub fn read_register(&self, reg: Register) -> u8 {
        let v = self.data.registers[reg];
        self.debugger.borrow_mut().read_register(reg, v);
        v
    }

    pub fn write_register(&mut self, reg: Register, value: u8) {
        self.data.registers[reg] = value;
        self.debugger.borrow_mut().write_register(reg, value);
    }

    pub fn read_pc(&self) -> MemoryAddress {
        self.data.pc
    }

    pub fn write_pc(&mut self, addr: MemoryAddress) -> Result<(), MemoryError> {
        self.in_program_range(addr)?;
        self.debugger.borrow_mut().write_pc(addr);
        self.data.pc = addr;
        Ok(())
    }

    pub fn push_pc(&mut self) -> Result<(), MemoryError> {
        if self.data.stack.len() == 12 {
            return Err(self.error("Stack overflow", None));
        }
        self.data.stack.push(self.read_pc());
        Ok(())
    }

    pub fn pop_pc(&mut self) -> Result<(), MemoryError> {
        match self.data.stack.pop() {
            Some(pc) => self.write_pc(pc),
            None => Err(self.error("Return without address on stack", None))
        }
    }

    pub fn write_i(&mut self, addr: MemoryAddress) {
        self.data.i = addr;
        self.debugger.borrow_mut().write_i(self.data.i);
    }

    pub fn read_i(&self) -> MemoryAddress {
        self.debugger.borrow_mut().read_i(self.data.i);
        self.data.i
    }

    pub fn read_ds(&self) -> u8 {
        // TODO update debugger for breakpoints?
        self.data.ds
    }

    pub fn write_ds(&mut self, ds: u8) {
        // TODO update debugger for breakpoints?
        self.data.ds = ds;
    }

    pub fn read_dt(&self) -> u8 {
        // TODO update debugger for breakpoints?
        self.data.dt
    }

    pub fn write_dt(&mut self, dt: u8) {
        // TODO update debugger for breakpoints?
        self.data.dt = dt;
    }

    pub fn read_random(&self) -> u8 {
        // TODO breakpoints?
        // TODO allow debugger to mock random values?
        random::<u8>()
    }

    pub fn error<S: Into<String>>(&self, reason: S, instruction: Option<Instruction>) -> MemoryError {
        MemoryError {
            pc: self.data.pc as MemoryAddress - 2,
            reason: reason.into(),
            instruction,
            i: self.data.i
        }
    }


    // Display ----------------------------------------------------------------
    pub fn write_display(&mut self, addr: MemoryAddress, v: u8) {
        self.debugger.borrow_mut().write_display(addr, v);
        self.data.pixels[addr as usize] = v;
    }

    pub fn read_display(&self, addr: MemoryAddress) -> u8 {
        let v = self.data.pixels[addr as usize];
        self.debugger.borrow_mut().read_display(addr, v);
        v
    }


    // Debugger ---------------------------------------------------------------
    pub fn stack(&self) -> &[MemoryAddress] {
        &self.data.stack
    }

    pub fn pc(&self) -> MemoryAddress {
        self.data.pc
    }

    pub fn set_pc(&mut self, v: i32) {
        self.data.pc = v.max(0).min(MAX_PROGRAM_ADDRESS as i32) as u16;
    }

    pub fn i(&self) -> MemoryAddress {
        self.data.i
    }

    pub fn set_i(&mut self, v: i32) {
        self.data.i = v.max(0).min(MAX_DATA_ADDRESS as i32) as u16;
    }

    pub fn dt(&self) -> u8 {
        self.data.dt
    }

    pub fn set_dt(&mut self, v: i32) {
        self.data.dt = v.max(0).min(255) as u8;
    }

    pub fn ds(&self) -> u8 {
        self.data.ds
    }

    pub fn set_ds(&mut self, v: i32) {
        self.data.ds = v.max(0).min(255) as u8;
    }

    pub fn reg(&self, r: Register) -> u8 {
        self.data.registers[r]
    }

    pub fn set_reg(&mut self, r: Register, v: i32) {
        self.data.registers[r] = v.max(0).min(255) as u8;
    }

}

impl Memory {

    fn in_program_range(&self, address: MemoryAddress) -> Result<(), MemoryError> {
        if address >= MAX_PROGRAM_ADDRESS {
            Err(self.error(format!("address 0x{:0>4X} exceeds PROGRAM space", self.data.pc + 1), None))

        } else {
            Ok(())
        }
    }

    fn in_address_range(&self, address: MemoryAddress) -> Result<(), MemoryError> {
        if address >= MAX_DATA_ADDRESS {
            Err(self.error(format!("address 0x{:0>4X} exceeds ADDRESS space", self.data.pc + 1), None))

        } else {
            Ok(())
        }
    }

}

