// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::memory::{Memory, MemoryAddress, MemoryError};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub const DISPLAY_WIDTH: u16 = 128;
pub const DISPLAY_HEIGHT: u16 = 64;
pub const DISPLAY_BUFFER_SIZE: usize = (DISPLAY_PIXELS_PER_PLANE * 2 + DISPLAY_HI_RES_FLAG) as usize;

const DISPLAY_PIXELS_PER_PLANE: u16 = DISPLAY_WIDTH * DISPLAY_HEIGHT;
const DISPLAY_PLANE_FLAG: u16 = DISPLAY_PIXELS_PER_PLANE * 2;
const DISPLAY_HI_RES_FLAG: u16 = DISPLAY_PIXELS_PER_PLANE * 2 + 1;


// Display Abstraction --------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Display;
impl Display {
    pub fn blit(memory: &Memory, frame: &mut [u8]) {
        for (i, pixel) in frame.chunks_exact_mut(4).enumerate() {
            let l = memory.read_display(i as u16);
            let h = memory.read_display((i as u16).saturating_add(DISPLAY_PIXELS_PER_PLANE));
            let rgba = match l | (h << 1) {
                1 => [0xff, 0x8c, 0x1f, 0xff],
                2 => [0xb8, 0x54, 0x32, 0xff],
                3 => [0xff, 0x8c, 0x1f, 0xff],
                _ => [0x23, 0x10, 0x04, 0xff]
            };
            pixel.copy_from_slice(&rgba);
        }
    }

    pub fn set_resolution(memory: &mut Memory, high: bool, clear: bool) {
        memory.write_display(DISPLAY_HI_RES_FLAG, if high { 1 } else { 0 });
        if clear {
            Self::clear(memory);
        }
    }

    pub fn clear(memory: &mut Memory) {
        let plane = memory.read_display(DISPLAY_PLANE_FLAG).saturating_add(1);
        for p in 1..3 {
            if (plane & p) != 0 {
                for i in 0..DISPLAY_PIXELS_PER_PLANE {
                    Self::write_pixel(memory, i, 0, p);
                }
            }
        }
    }

    pub fn set_plane(memory: &mut Memory, plane: u8) {
        memory.write_display(DISPLAY_PLANE_FLAG, plane.saturating_sub(1));
    }

    pub fn draw_sprite(memory: &mut Memory, x: u16, y: u16, len: u16, mut addr: MemoryAddress) -> Result<u8, MemoryError> {
        let mut flipped= 0;
        let plane = memory.read_display(DISPLAY_PLANE_FLAG).saturating_add(1);
        for p in 1..3u8 {
            if (plane & p) == p {
                // Switch between standard 8x8 or S-Chip 16x16 sprites
                let (height, bits, bytes) = if len == 0 {
                    (16, 16, 32)

                } else {
                    (len, 8, len)
                };

                for h in 0..height {
                    // 1 Byte = 8 Bits per row
                    for b in 0..bits {

                        // TODO clip an set bit to 0

                        // Check if bit is set
                        let bit = if b >= 8 {
                            memory.read(addr + (h * 2 + 1))? & (0x80 >> (b & 0x07))

                        } else if height == 16 {
                            memory.read(addr + (h * 2))? & (0x80 >> b)

                        } else {
                            memory.read(addr + h)? >> (7 - b) & 0x01
                        };
                        if bit != 0 {
                            flipped |= Self::set_pixel(memory, x + b, y + h, p);
                        }
                    }
                }
                addr += bytes;
            }
        }
        Ok(flipped)
    }

    pub fn scroll_down(memory: &mut Memory, n: u8) {
        // TODO handle planes
        let n = n as u16;
        for y in 0..DISPLAY_HEIGHT {
            for x in 0..DISPLAY_WIDTH {
                if y < DISPLAY_HEIGHT.saturating_sub(n) {
                    let p = memory.read_display((y.saturating_add(n)) * DISPLAY_WIDTH + x);
                    memory.write_display(y * DISPLAY_WIDTH + x, p);

                } else {
                    memory.write_display(y * DISPLAY_WIDTH + x, 0);
                }
            }
        }
    }

    pub fn scroll_left(memory: &mut Memory) {
        // TODO handle planes
        for y in 0..DISPLAY_HEIGHT {
            for x in 0..DISPLAY_WIDTH - 4 {
                let p = memory.read_display(y * DISPLAY_WIDTH + x + 4);
                memory.write_display(y * DISPLAY_WIDTH + x, p);
            }
            memory.write_display(y * DISPLAY_WIDTH + DISPLAY_WIDTH - 1, 0);
            memory.write_display(y * DISPLAY_WIDTH + DISPLAY_WIDTH - 2, 0);
            memory.write_display(y * DISPLAY_WIDTH + DISPLAY_WIDTH - 3, 0);
            memory.write_display(y * DISPLAY_WIDTH + DISPLAY_WIDTH - 4, 0);
        }
    }

    pub fn scroll_right(memory: &mut Memory) {
        // TODO handle planes
        for y in 0..DISPLAY_HEIGHT {
            for x in 0..DISPLAY_WIDTH - 4 {
                let x = DISPLAY_WIDTH - x - 1;
                let p = memory.read_display(y * DISPLAY_WIDTH + x - 4);
                memory.write_display(y * DISPLAY_WIDTH + x, p);
            }
            memory.write_display(y * DISPLAY_WIDTH, 0);
            memory.write_display(y * DISPLAY_WIDTH + 1, 0);
            memory.write_display(y * DISPLAY_WIDTH + 2, 0);
            memory.write_display(y * DISPLAY_WIDTH + 3, 0);
        }
    }

    fn set_pixel(memory: &mut Memory, x: u16, y: u16, p: u8) -> u8 {
        let is_high_res = memory.read_display(DISPLAY_HI_RES_FLAG) == 1;
        let (max_width, max_height) = if is_high_res {
            (0x7F, 0x3F)

        } else {
            (0x3F, 0x1F)
        };

        let tx = x & max_width;
        let ty = y & max_height;
        let l_index = ty * 128 * 2 + tx * 2;
        let h_index = ty * 128 + tx;
        let index = if is_high_res {
            h_index

        } else {
            l_index
        };

        let (color, toggle) = if Self::read_pixel(memory, index, p) == 1 {
            (0, 1)

        } else {
            (1, 0)
        };

        if is_high_res {
            Self::write_pixel(memory, h_index, color, p);

        } else {
            Self::write_pixel(memory, l_index, color, p);
            Self::write_pixel(memory, l_index + 1, color, p);
            Self::write_pixel(memory, l_index + 128, color, p);
            Self::write_pixel(memory, l_index + 128 + 1, color, p);
        }
        toggle
    }

    fn write_pixel(memory: &mut Memory, i: u16, value: u8, p: u8) {
        match p {
            1 => memory.write_display(i, value),
            2 => memory.write_display(i.saturating_add(DISPLAY_PIXELS_PER_PLANE), value),
            _ => {}
        }
    }

    fn read_pixel(memory: &mut Memory, i: u16, p: u8) -> u8 {
        match p {
            1 => memory.read_display(i),
            2 => memory.read_display(i.saturating_add(DISPLAY_PIXELS_PER_PLANE)),
            _ => 0
        }
    }

}

