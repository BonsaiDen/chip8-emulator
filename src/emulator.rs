// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::path::PathBuf;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use app::{Application, AudioEvent, imgui, Input, Fullscreen, LogicalSize, Key, Window};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::vm::VM;
use crate::display::{Display, DISPLAY_WIDTH, DISPLAY_HEIGHT};
use crate::memory::{Memory, MemoryAddress, MemoryError};
use crate::debugger::{Debugger, DebuggerCell};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub const EMULATOR_TITLE: &'static str = "Chip-8 Emulator";
pub const EMULATOR_WIDTH: u32 = 512;
pub const EMULATOR_HEIGHT: u32 = 256;

const EMULATOR_AUDIO_SAMPLE_RATE: usize = 44100;
const EMULATOR_AUDIO_VOLUME: f32 = 0.25;
const EMULATOR_AUDIO_FREQ: usize = 4000;
const EMULATOR_TIMER_FREQ: usize = 60;

const STANDARD_SPEED: u16 = 1000;
const SLOW_SPEED: u16 = 20;

const KEY_LIST: [(Key, usize); 21] = [
    (Key::Key1, 1),
    (Key::Key2, 2),
    (Key::Key3, 3),
    (Key::Q, 4),
    (Key::W, 5), (Key::Up, 5),
    (Key::E, 6), (Key::Space, 6),
    (Key::A, 7), (Key::Left, 7),
    (Key::S, 8), (Key::Down, 8),
    (Key::D, 9), (Key::Right, 9),
    (Key::X, 0xA),
    (Key::Y, 0),
    (Key::C, 0xB),
    (Key::Key4, 0xC),
    (Key::R, 0xD),
    (Key::F, 0xE),
    (Key::V, 0xF)
];

const LOW_RES_FONT: [u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // '0'
    0x20, 0x60, 0x20, 0x20, 0x70, // '1'
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // '2'
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // '3'
    0x90, 0x90, 0xF0, 0x10, 0x10, // '4'
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // '5'
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // '6'
    0xF0, 0x10, 0x20, 0x40, 0x40, // '7'
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // '8'
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // '9'
    0xF0, 0x90, 0xF0, 0x90, 0x90, // 'A'
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // 'B'
    0xF0, 0x80, 0x80, 0x80, 0xF0, // 'C'
    0xE0, 0x80, 0x80, 0x80, 0xE0, // 'D'
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // 'E'
    0xF0, 0x80, 0xF0, 0x80, 0x80, // 'F'
];

const HIGH_RES_FONT: [u8; 160] = [
    0x7C, 0x82, 0x82, 0x82, 0x82, 0x82, 0x82, 0x82, 0x7C, 0x00, // '0'
    0x08, 0x18, 0x38, 0x08, 0x08, 0x08, 0x08, 0x08, 0x3C, 0x00, // '1'
    0x7C, 0x82, 0x02, 0x02, 0x04, 0x18, 0x20, 0x40, 0xFE, 0x00, // '2'
    0x7C, 0x82, 0x02, 0x02, 0x3C, 0x02, 0x02, 0x82, 0x7C, 0x00, // '3'
    0x84, 0x84, 0x84, 0x84, 0xFE, 0x04, 0x04, 0x04, 0x04, 0x00, // '4'
    0xFE, 0x80, 0x80, 0x80, 0xFC, 0x02, 0x02, 0x82, 0x7C, 0x00, // '5'
    0x7C, 0x82, 0x80, 0x80, 0xFC, 0x82, 0x82, 0x82, 0x7C, 0x00, // '6'
    0xFE, 0x02, 0x04, 0x08, 0x10, 0x20, 0x20, 0x20, 0x20, 0x00, // '7'
    0x7C, 0x82, 0x82, 0x82, 0x7C, 0x82, 0x82, 0x82, 0x7C, 0x00, // '8'
    0x7C, 0x82, 0x82, 0x82, 0x7E, 0x02, 0x02, 0x82, 0x7C, 0x00, // '9'
    0x10, 0x28, 0x44, 0x82, 0x82, 0xFE, 0x82, 0x82, 0x82, 0x00, // 'A'
    0xFC, 0x82, 0x82, 0x82, 0xFC, 0x82, 0x82, 0x82, 0xFC, 0x00, // 'B'
    0x7C, 0x82, 0x80, 0x80, 0x80, 0x80, 0x80, 0x82, 0x7C, 0x00, // 'C'
    0xFC, 0x82, 0x82, 0x82, 0x82, 0x82, 0x82, 0x82, 0xFC, 0x00, // 'D'
    0xFE, 0x80, 0x80, 0x80, 0xF8, 0x80, 0x80, 0x80, 0xFE, 0x00, // 'E'
    0xFE, 0x80, 0x80, 0x80, 0xF8, 0x80, 0x80, 0x80, 0x80, 0x00, // 'F'
];


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait EmulatorWindow {
    fn scale(&self) -> u32;
    fn set_scale(&self, scale: u32);
}

impl EmulatorWindow for Window {
    fn scale(&self) -> u32 {
        let size = self.inner_size();
        let size: LogicalSize<u32> = LogicalSize::from_physical(size, self.scale_factor());
        (size.width / EMULATOR_WIDTH).min(size.height / EMULATOR_HEIGHT)
    }

    fn set_scale(&self, scale: u32) {
        let size = LogicalSize::new(EMULATOR_WIDTH * scale, EMULATOR_HEIGHT * scale);
        self.set_inner_size(size);
    }
}


// Structs / Enums ------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Default)]
struct EmulatorAudio {
    pattern: Option<[u8; 16]>,
    events: Vec<AudioEvent>
}

impl EmulatorAudio {
    fn set_pattern(&mut self, pattern: [u8; 16]) {
        self.pattern = Some(pattern);
    }

    fn fill_buffer(&mut self, length: u8, remaining: u8) {
        // De-queue remaining samples in case the timer is set to zero in order
        // to correctly stop the sound
        if remaining > 0 {
            self.events.push(AudioEvent::Dequeue(
                remaining as usize * EMULATOR_AUDIO_SAMPLE_RATE / EMULATOR_TIMER_FREQ
            ))
        }

        // Fill next audio buffer with sound data
        let samples = 16 * 8 * EMULATOR_AUDIO_SAMPLE_RATE / EMULATOR_AUDIO_FREQ;
        let mut buffer: Vec<f32> = Vec::with_capacity(samples);
        if let Some(pattern) = self.pattern.take() {
            for i in 0..samples {
                let index = i * EMULATOR_AUDIO_FREQ / EMULATOR_AUDIO_SAMPLE_RATE;
                let cell = index >> 3;
                let bit = index & 7;
                let volume = if pattern[cell] & (0x80 >> bit) != 0 {
                    EMULATOR_AUDIO_VOLUME

                } else {
                    0.0
                };
                buffer.push(volume);
            }
        } else {
            for _ in 0..samples {
                buffer.push(EMULATOR_AUDIO_VOLUME);
            }
        }
        self.events.push(AudioEvent::Queue(
            length as usize * EMULATOR_AUDIO_SAMPLE_RATE / EMULATOR_TIMER_FREQ,
            buffer
        ));
    }
}

pub enum EmulatorEvent {
    ClearScreen,
    LowRes(bool),
    HighRes(bool),
    AudioPattern([u8; 16]),
    AudioBuzz(u8, u8),
    DrawSprite(u8, u8, u8, MemoryAddress),
    ScrollDown(u8),
    Bitplane(u8),
    ScrollRight,
    ScrollLeft,
    Exit,
}


// Emulator Application -------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Emulator {
    rom: Option<(PathBuf, Vec<u8>)>,
    window_scale: Option<u32>,
    debugger: DebuggerCell,
    memory: Memory,
    audio: EmulatorAudio,
    vm: VM,
    speed: u16
}

impl Emulator {
    pub fn new(window_scale: u32) -> Self {
        let debugger = Debugger::new_cell();
        let memory = Memory::new(debugger.clone());
        Self {
            rom: None,
            window_scale: Some(window_scale),
            debugger,
            memory,
            audio: EmulatorAudio::default(),
            vm: VM::new(),
            speed: STANDARD_SPEED
        }
    }

    pub fn start(self) {
        Emulator::run(
            EMULATOR_TITLE,
            EMULATOR_WIDTH,
            EMULATOR_HEIGHT,
            DISPLAY_WIDTH as u32,
            DISPLAY_HEIGHT as u32,
            Some(EMULATOR_AUDIO_SAMPLE_RATE as u32),
            self
        );
    }

    pub fn debug(&mut self) {
        self.debugger.borrow_mut().enable();
        self.debugger.borrow_mut().pause("Paused via command line flag");
        self.debugger.borrow_mut().show();
        self.window_scale = Some(2);
    }

    pub fn load_rom_from_path(&mut self, path: PathBuf) -> Result<(), String> {
        let bytes = std::fs::read(&path).map_err(|e| e.to_string())?;
        self.rom = Some((path, bytes));
        self.reset();
        Ok(())
    }

    pub fn reset(&mut self) {
        self.memory.reset();
        for (i, b) in LOW_RES_FONT.iter().enumerate() {
            self.memory.write(i as MemoryAddress, *b).expect("font setup failed");
        }
        for (i, b) in HIGH_RES_FONT.iter().enumerate() {
            self.memory.write(i as MemoryAddress + 0x80, *b).expect("font setup failed");
        }
        self.vm.reset();
        if let Some((_, bytes)) = self.rom.as_ref() {
            self.memory.load_rom(bytes);
        }
    }

    fn update_inner(&mut self, input: &Input, _dt: f32) -> Result<(), MemoryError> {
        // TODO delta time for multiplication with speed
        let cycles = if self.debugger.borrow().is_paused() {
            // TODO debugger must handle keyboard when paused!

            // TODO clean up
            let reverts = self.debugger.borrow_mut().reverts();
            if let Some(reverts) = reverts {
                for _ in 0..reverts {
                    self.memory.revert();
                }
            }
            self.debugger.borrow_mut().cycles()

        } else {
            Self::update_keyboard(&mut self.memory, input);
            self.speed
        };

        let audio = &mut self.audio;
        self.vm.update(
            cycles,
            self.speed,
            &mut self.memory,
            |memory, event| {
                match event {
                    EmulatorEvent::ClearScreen => Display::clear(memory),
                    EmulatorEvent::ScrollDown(n) => Display::scroll_down(memory, n),
                    EmulatorEvent::ScrollLeft => Display::scroll_left(memory),
                    EmulatorEvent::ScrollRight => Display::scroll_right(memory),
                    EmulatorEvent::LowRes(c) => Display::set_resolution(memory, false, c),
                    EmulatorEvent::HighRes(c) => Display::set_resolution(memory, true, c),
                    EmulatorEvent::DrawSprite(x, y, height, addr) => {
                        let flipped = Display::draw_sprite(
                            memory,
                            x as u16,
                            y as u16,
                            height as u16,
                            addr
                        )?;
                        memory.set_flag_register(flipped);
                    },
                    EmulatorEvent::Bitplane(p) => Display::set_plane(memory, p),
                    EmulatorEvent::AudioPattern(pattern) => {
                        audio.set_pattern(pattern);
                    },
                    EmulatorEvent::AudioBuzz(length, remaining) => {
                        audio.fill_buffer(length, remaining);
                    },
                    EmulatorEvent::Exit => {
                        // TODO set running to false
                    }
                }
                Ok(())
            }
        )?;

        if input.key_pressed(Key::Back) {
            if self.speed == STANDARD_SPEED {
                self.speed = SLOW_SPEED;

            } else {
                self.speed = STANDARD_SPEED;
            }
        }

        Ok(())
    }

    fn update_keyboard(memory: &mut Memory, input: &Input) {
        for (code, index) in &KEY_LIST {
            if input.key_pressed(*code) && !input.key_pressed(Key::LShift) {
                memory.write_key(*index as u8, true);
            }
            if input.key_released(*code) {
                memory.write_key(*index as u8, false);
            }
        }
    }

}

impl Application for Emulator {
    fn init(&mut self, window: &Window) {
        if let Some((path, _)) = self.rom.as_ref() {
            if let Some(file_name) = path.file_name().and_then(|s| s.to_str()) {
                window.set_title(&format!("{} - {}", EMULATOR_TITLE, file_name));
            }
        }
    }

    fn audio_events(&mut self) -> Vec<AudioEvent> {
        self.audio.events.drain(0..).collect()
    }

    fn draw_pixels(&mut self, frame: &mut [u8]) {
        Display::blit(&self.memory, frame);
    }

    fn draw_ui(&mut self, ui: &imgui::Ui, input: &Input) {
        self.debugger.borrow_mut().draw(ui, input, &mut self.memory);
    }

    fn update(&mut self, window: &Window, input: &Input, dt: f32) -> Result<bool, String> {

        // Reset
        if (input.key_pressed(Key::R) && input.key_held(Key::LShift)) || self.debugger.borrow_mut().should_reset() {
            self.reset();
        }

        if input.key_pressed(Key::Return) && input.key_held(Key::LAlt) {
            if window.fullscreen().is_none() {
                window.set_fullscreen(Some(Fullscreen::Borderless(window.primary_monitor())));

            } else {
                window.set_fullscreen(None);
            }
        }

        // Handle Window Scaling
        if input.key_pressed(Key::F1) {
            self.window_scale = Some(1);

        } else if input.key_pressed(Key::F2) {
            self.window_scale = Some(2);

        } else if input.key_pressed(Key::F3) {
            self.window_scale = Some(3);
        }

        if let Some(scale) = self.window_scale.take() {
            window.set_scale(scale);
        }

        if self.rom.is_some() {
            match self.update_inner(input, dt) {
                Ok(()) => {},
                Err(err) => if self.debugger.borrow().is_enabled() {
                    self.debugger.borrow_mut().pause(err.reason);

                } else {
                    return Err(format!("{:#?}", err));
                }
            }
        }
        self.debugger.borrow_mut().update(window, input);
        Ok(true)
    }

}

