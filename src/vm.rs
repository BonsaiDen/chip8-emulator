// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use num_traits::FromPrimitive;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::emulator::EmulatorEvent;
use crate::memory::{Memory, MemoryAddress, MemoryError, Register};


#[derive(Debug, Copy, Clone)]
pub struct Instruction(pub u16);

impl Instruction {
    pub fn from_u16(word: u16) -> Self {
        Instruction(word)
    }

    pub fn decode(&self) -> (u8, u16, u8, u8, Register, Register) {
        (
            (self.0 >> 12) as u8,
            self.0 & 0x0FFF,
            (self.0 & 0x00FF) as u8,
            (self.0 & 0x000F) as u8,
            Register::from_u16((self.0 & 0x0F00) >> 8).unwrap_or(Register::V0),
            Register::from_u16((self.0 & 0x00F0) >> 4).unwrap_or(Register::V0)
        )
    }

    pub fn is_long(&self) -> bool {
        self.0 == 0xF000
    }

    pub fn is_return(&self) -> bool {
        self.0 == 0x00EE
    }

    pub fn is_skip(&self) -> bool {
        self.0 >> 12 == 0x09 || self.0 >> 12 == 0x04 ||
        self.0 >> 12 == 0x03 || self.0 == 0xE09E ||
        self.0 == 0xE0A1
    }

    pub fn jump_target(&self) -> Option<MemoryAddress> {
        if self.0 >> 12 == 0x01 {
            Some(self.0 & 0x0FFF)

        } else {
            None
        }
    }

    pub fn call_target(&self) -> Option<MemoryAddress> {
        if self.0 >> 12 == 0x02 {
            Some(self.0 & 0x0FFF)

        } else {
            None
        }
    }

    pub fn mnemonic(&self) -> &'static str {
        match self.0 & 0xF0FF {
            0x00C0..=0x00CF => " SCD",
            0x00D0..=0x00DF => " SCU",
            0x00E0 => " CLS",
            0x00EE => " RET",
            0x00FB => " SCR",
            0x00FC => " SCL",
            0x00FD => "EXIT",
            0x00FE => " LOW",
            0x00FF => "HIGH",
            0xE09E => " SKP Vx",
            0xE0A1 => "SKNP Vx",
            0xF000 => "  LD I, NNN",
            0xF002 => "  LD AUD, [I]",
            0xF007 => "  LD Vx, DT",
            0xF00A => "  LD Vx, K",
            0xF015 => "  LD DT, Vx",
            0xF018 => "  LD ST, Vx",
            0xF01E => " ADD I, Vx",
            0xF029 => "  LD LF, Vx",
            0xF030 => "  LD HF, Vx",
            0xF033 => "  LD BCD, Vx",
            0xF055 => "  LD [I], Vx",
            0xF065 => "  LD Vx, [I]",
            _ => match self.0 >> 12 {
                0x1 => "  JP NNN",
                0x2 => "CALL NNN",
                0x3 => "  SE Vx, NN",
                0x4 => " SNE Vx, NN",
                0x5 => match self.0 & 0x00F {
                    0x0 => "  SE Vx, Vy",
                    0x2 => "  LD [I], Vx, Vy",
                    0x3 => "  LD Vx, Vy, [I]",
                    _ => "  ??"
                },
                0x6 => "  LD Vx, NN",
                0x7 => " ADD Vx, NN",
                0x8 => match self.0 & 0x000F {
                    0x0 => "  LD Vx, Vy",
                    0x1 => "  OR Vx, Vy",
                    0x2 => " AND Vx, Vy",
                    0x3 => " XOR Vx, Vy",
                    0x4 => " ADD Vx, Vy",
                    0x5 => " SUB Vx, Vy",
                    0x6 => " SHR Vx, Vy",
                    0x7 => "SUBN Vx, Vy",
                    0xE => " SHL Vx, Vy",
                    _ => "  ??"
                },
                0x9 => " SNE Vx, Vy",
                0xA => "  LD I, NNN",
                0xB => "  JP V0, NNN",
                0xC => " RND Vx, NN",
                0xD => "DRAW Vx, Vy, ZZ, I",
                0xF if self.0 & 0x00FF == 0x01 => " BPL BB",
                _ => "  ??"
            }
        }
    }
}


// VM Abstraction -------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct VM {
    pub timer_cycles: u16
}

impl VM {

    pub fn new() -> Self {
        Self {
            timer_cycles: 0
        }
    }

    pub fn reset(&mut self) {
        self.timer_cycles = 0;
    }

    pub fn update<C: FnMut(&mut Memory, EmulatorEvent) -> Result<(), MemoryError>>(
        &mut self,
        cycles: u16,
        cycles_per_frame: u16,
        memory: &mut Memory,
        mut handler: C

    ) -> Result<(), MemoryError> {
        for _ in 0..cycles {
            self.timer_cycles = self.timer_cycles.saturating_add(1);
            match self.step(memory) {
                Ok(Some(event)) => handler(memory, event)?,
                Ok(None) => {},
                Err(err) => return Err(err)
            }
        }
        while self.timer_cycles >= cycles_per_frame {
            self.timer_cycles = self.timer_cycles.saturating_sub(cycles_per_frame);
            memory.write_dt(memory.read_dt().saturating_sub(1));
            memory.write_ds(memory.read_ds().saturating_sub(1));
        }
        Ok(())
    }

    fn step(
        &mut self,
        memory: &mut Memory

    ) -> Result<Option<EmulatorEvent>, MemoryError> {
        let instr = memory.read_instruction()?;
        match self.execute(memory, instr) {
            Ok(event) => {
                Ok(event)
            },
            Err(err) => Err(err)
        }
    }
}

impl VM {
    fn execute(
        &mut self,
        memory: &mut Memory,
        i: Instruction

    ) -> Result<Option<EmulatorEvent>, MemoryError> {
        let (mnemonic, nnn, nn, n, x, y) = i.decode();
        match mnemonic {
            0x0 => return self.system(memory, i),
            // Jumps to address NNN
            0x1 => {
                memory.write_pc(nnn)?;
            },
            // Calls subroutine at NNN.
            0x2 => {
                memory.push_pc()?;
                memory.write_pc(nnn)?;
            },
            // Skips the next instruction if VX equals NN. (Usually the next instruction is a jump to skip a code block)
            0x3 => if memory.read_register(x) == nn {
                memory.skip_instruction()?;
            },
            // Skips the next instruction if VX doesn't equal NN. (Usually the next instruction is a jump to skip a code block)
            0x4 => if memory.read_register(x) != nn {
                memory.skip_instruction()?;
            },
            0x5 => match n {
                // Skips the next instruction if VX equals VY.
                // (Usually the next instruction is a jump to skip a code block)
                0x0 => if memory.read_register(x) == memory.read_register(y) {
                    memory.skip_instruction()?;
                },
                // Save VX..VY to memory starting at I (same as CHIP-8E);
                // order can be ascending or descending; does not increment I
                0x2 => {
                    let mut i = 0;
                    let x = x as u16;
                    let y = y as u16;
                    if y >= x {
                        for r in x..=y {
                            let r = Register::from_u16(r).unwrap();
                            self.register_to_memory(memory, r, memory.read_i() + i)?;
                            i += 1;
                        }

                    } else {
                        unimplemented!();
                    }
                },
                // Load VX..VY from memory starting at I (same as CHIP-8E);
                // order can be ascending or descending; does not increment I
                0x3 => {
                    let mut i = 0;
                    let x = x as u16;
                    let y = y as u16;
                    if y >= x {
                        for r in x..=y {
                            let r = Register::from_u16(r).unwrap();
                            self.memory_to_register(memory, memory.read_i() + i, r)?;
                            i += 1;
                        }

                    } else {
                        unimplemented!();
                    }
                },
                _ => return self.invalid_instruction(memory, i)
            },
            // Sets VX to NN.
            0x6 => memory.write_register(x, nn),
            // Adds NN to VX. (Carry flag is not changed)
            0x7 => {
                let v = memory.read_register(x);
                memory.write_register(x, v.wrapping_add(nn));
            },
            0x8 => return self.math(memory, i),
            // Skips the next instruction if VX doesn't equal VY.
            // (Usually the next instruction is a jump to skip a code block)
            0x9 => if memory.read_register(x) != memory.read_register(y) {
                memory.skip_instruction()?;
            },
            // Sets I to the address NNN.
            0xA => memory.write_i(nnn),
            // Jumps to the address NNN plus V0.
            0xB => {
                let v = memory.read_register(Register::V0);
                let addr = nnn + v as MemoryAddress;
                memory.write_pc(addr)?;
            },
            // Sets VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN.
            0xC => memory.write_register(x, memory.read_random() & nn),
            // Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N+1 pixels.
            // Each row of 8 pixels is read as bit-coded starting from memory location I;
            //
            // I value doesn’t change after the execution of this instruction. As described above,
            // VF is set to 1 if any screen pixels are flipped from set to unset when the sprite is drawn,
            // and to 0 if that doesn’t happenDraws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N+1 pixels.
            //
            // Each row of 8 pixels is read as bit-coded starting from memory location I;
            // I value doesn’t change after the execution of this instruction.
            //
            // As described above, VF is set to 1 if any screen pixels are flipped from
            // set to unset when the sprite is drawn, and to 0 if that doesn’t happen
            0xD => return Ok(Some(EmulatorEvent::DrawSprite(
                memory.read_register(x),
                memory.read_register(y),
                n,
                memory.read_i()
            ))),
            0xE => match nn {
                // Skips the next instruction if the key stored in VX is pressed.
                // (Usually the next instruction is a jump to skip a code block)
                0x9E => if memory.read_key(memory.read_register(x)) {
                    memory.skip_instruction()?;
                },
                // Skips the next instruction if the key stored in VX isn't pressed.
                // (Usually the next instruction is a jump to skip a code block)
                0xA1 => if !memory.read_key(memory.read_register(x)) {
                    memory.skip_instruction()?;
                },
                _ => return self.invalid_instruction(memory, i)
            },
            0xF => match nn {
                // Load I with 16-bit address NNNN
                0x00 if nnn == 0 => {
                    let addr = memory.read_u16()?;
                    memory.write_i(addr);
                },
                // XO - Select Bitplane
                0x01 => return Ok(Some(EmulatorEvent::Bitplane((nnn >> 8) as u8 & 0x03))),
                // XO - F002: Store 16 bytes in audio pattern buffer,
                // starting at I, to be played by the sound buzzer
                0x02 => {
                    let mut pattern = [0u8; 16];
                    for i in 0..16 {
                        pattern[i] = memory.read(memory.read_i().wrapping_add(i as u16))?;
                    }
                    return Ok(Some(EmulatorEvent::AudioPattern(pattern)));
                },
                // Sets VX to the value of the delay timer.
                0x07 => memory.write_register(x, memory.read_dt()),
                // A key press is awaited, and then stored in VX.
                // (Blocking Operation. All instruction halted until next key event)
                0x0A => if let Some(key) = memory.read_any_key() {
                    memory.write_register(x, key);

                } else {
                    memory.write_pc(memory.read_pc().saturating_sub(2))?;
                },
                // Sets the delay timer to VX.
                0x15 => memory.write_dt(memory.read_register(x)),
                // Sets the sound timer to VX and start the sound buzzer.
                0x18 => {
                    let v = memory.read_register(x);
                    let ds = memory.read_ds();
                    memory.write_ds(v);
                    return Ok(Some(EmulatorEvent::AudioBuzz(v, ds)));
                },
                // Adds VX to I. VF is not affected.
                0x1E => {
                    // TODO S8 - set flag if address overflows
                    let i = memory.read_i();
                    let i = i.wrapping_add(memory.read_register(x) as MemoryAddress);
                    memory.write_i(i)
                },
                // Sets I to the location of the sprite for the character in VX.
                // Characters 0-F (in hexadecimal) are represented by a 4x5 font.
                0x29 => {
                    let i = (memory.read_register(x) & 0x0F) as MemoryAddress * 5;
                    memory.write_i(i)
                },
                // Sets I to the location of the sprite for the character in VX.
                // Characters 0-F (in hexadecimal) are represented by a 8x10 font.
                0x30 => {
                    let i = ((memory.read_register(x) & 0x0F) as MemoryAddress * 10) + 80;
                    memory.write_i(i)
                },
                // Stores the binary-coded decimal representation of VX, with the most significant of three digits at the address in I,
                // the middle digit at I plus 1, and the least significant digit at I plus 2.
                // (In other words, take the decimal representation of VX, place the hundreds digit in memory at location in I, the tens digit at location I+1, and the ones digit at location I+2.)
                0x33 => {
                    let b = (memory.read_register(x) / 100) % 10;
                    memory.write(memory.read_i(), b)?;
                    let c = (memory.read_register(x) / 10) % 10;
                    memory.write(memory.read_i() + 1, c)?;
                    let d = memory.read_register(x) % 10;
                    memory.write(memory.read_i() + 2, d)?;
                },
                // Stores V0 to VX (including VX) in memory starting at address I.
                // The offset from I is increased by 1 for each value written, but I itself is left unmodified.
                0x55 => for i in 0..=x as u16 {
                    // let value = self.read_register(debugger, r);
                    // memory.write(self.i + i, value)?;
                    let r = Register::from_u16(i).unwrap();
                    self.register_to_memory(memory, r, memory.read_i() + i)?;
                },
                // Fills V0 to VX (including VX) with values from memory starting at address I.
                // The offset from I is increased by 1 for each value written, but I itself is left unmodified.
                0x65 => for i in 0..=x as u16 {
                    let r = Register::from_u16(i).unwrap();
                    self.memory_to_register(memory, memory.read_i() + i, r)?;
                },
                // TODO S8 0x75 / 0x85 store permanent flags
                _ => return self.invalid_instruction(memory, i)
            },
            _ => return self.invalid_instruction(memory, i)
        }
        Ok(None)
    }

    fn system(&mut self, memory: &mut Memory, i: Instruction) -> Result<Option<EmulatorEvent>, MemoryError> {
        let (_, nnn, _, _, _, _) = i.decode();
        Ok(match nnn {
            // Scroll Screen
            0x0C0..=0xCF => Some(EmulatorEvent::ScrollDown((nnn & 0xF) as u8)),
            // TODO XO - 00DN: Scroll up N pixels
            // Clear Screen
            0x0E0 => Some(EmulatorEvent::ClearScreen),
            // Returns from a subroutine
            0x0EE => {
                memory.pop_pc()?;
                None
            },
            0x0FB => Some(EmulatorEvent::ScrollRight),
            0x0FC => Some(EmulatorEvent::ScrollLeft),
            // Exit Interpreter
            0x0FD => Some(EmulatorEvent::Exit),
            // Switch to Low Resolution Mode
            // TODO XO - 00FE and 00FF, which switch between low and high resolution, will clear the screen as well.
            0x0FE => Some(EmulatorEvent::LowRes(false)),
            // Switch to High Resolution Mode
            0x0FF => Some(EmulatorEvent::HighRes(false)),
            _ => return self.invalid_instruction(memory, i)
        })
    }

    fn math(&mut self, memory: &mut Memory, i: Instruction) -> Result<Option<EmulatorEvent>, MemoryError> {
        let (_, _, _, n, x, y) = i.decode();
        match n {
            // Sets VX to the value of VY.
            0x0 => {
                let v = memory.read_register(y);
                memory.write_register(x, v);
            },
            // Sets VX to VX or VY. (Bitwise OR operation)
            0x1 => {
                let v = memory.read_register(x) | memory.read_register(y);
                memory.write_register(x, v);
            },
            // Sets VX to VX and VY. (Bitwise AND operation)
            0x2 => {
                let v = memory.read_register(x) & memory.read_register(y);
                memory.write_register(x, v);
            },
            // Sets VX to VX xor VY.
            0x3 => {
                let v = memory.read_register(x) ^ memory.read_register(y);
                memory.write_register(x, v);
            },
            // Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.
            0x4 => {
                let c = if (memory.read_register(x) as u16 + memory.read_register(y) as u16) > 0xFF { 1 } else { 0 };
                let v = memory.read_register(x).wrapping_add(memory.read_register(y));
                memory.write_register(x, v);
                memory.write_register(Register::VF, c);
            },
            // VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
            0x5 => {
                let c = if memory.read_register(x) >= memory.read_register(y) { 1 } else { 0 };
                let v = memory.read_register(x).wrapping_sub(memory.read_register(y));
                memory.write_register(x, v);
                memory.write_register(Register::VF, c);
            },
            // Stores the least significant bit of VX in VF and then shifts VX to the right by 1.
            0x6 => {
                let c = memory.read_register(x) & 0b1;
                let v = memory.read_register(x) >> 1;
                memory.write_register(x, v);
                memory.write_register(Register::VF, c);
            },
            // Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
            0x7 => {
                let c = if memory.read_register(y) >= memory.read_register(x) { 1 } else { 0 };
                let v = memory.read_register(y).wrapping_sub(memory.read_register(x));
                memory.write_register(x, v);
                memory.write_register(Register::VF, c);
            },
            // Stores the most significant bit of VX in VF and then shifts VX to the left by 1.
            0xE => {
                let c = (memory.read_register(x) & 0b1000_0000) >> 7;
                let v = memory.read_register(x) << 1;
                memory.write_register(x, v);
                memory.write_register(Register::VF, c);
            },
            _ => return self.invalid_instruction(memory, i)
        }
        Ok(None)
    }

    fn register_to_memory(&self, memory: &mut Memory, reg: Register, addr: MemoryAddress) -> Result<(), MemoryError> {
        let value = memory.read_register(reg);
        memory.write_with_register(addr, reg, value)
    }

    fn memory_to_register(&mut self, memory: &mut Memory, addr: MemoryAddress, reg: Register) -> Result<(), MemoryError> {
        let value = memory.read(addr)?;
        memory.write_register(reg, value);
        Ok(())
    }

    fn invalid_instruction(&self, memory: &Memory, i: Instruction) -> Result<Option<EmulatorEvent>, MemoryError> {
        Err(memory.error("Invalid instruction", Some(i)))
    }

}

